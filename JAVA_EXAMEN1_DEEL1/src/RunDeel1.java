import fotoIO.Foto;
import fotoIO.FotoAlbum;
import fotoIO.FotoData;

import java.util.List;

/**
 * IN DEZE KLASSE NIETS VERANDEREN!
 */
public class RunDeel1 {
    public static void main(String[] args) {
        //Fotoalbum klaarmaken:
        List<Foto> fotoList = FotoData.getFotoList();
        FotoAlbum album = new FotoAlbum("Karel de Grote");
        for (Foto foto : fotoList) {
            album.voegToe(foto);
        }
        System.out.println("\nNa aanmaken fotoalbum:");
        System.out.println(album);
    }
}
