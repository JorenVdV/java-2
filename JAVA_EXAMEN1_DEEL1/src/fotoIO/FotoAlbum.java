package fotoIO;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 */
public class FotoAlbum {
    private final String klantId;
    private TreeSet<Foto> fotoSet;

    public FotoAlbum(String klantId) {
        this.klantId = klantId;
        //Opgave 1.4
        fotoSet = new TreeSet<Foto>(new FotoComparator());
    }

    public String getKlantId() {
        return klantId;
    }

    public void voegToe(Foto newFoto) {
        //Opgave 1.5
    	if(!fotoSet.contains(newFoto)){
    		if(fotoSet.stream().
    				filter(e->e.getPaginaNummer()==newFoto.getPaginaNummer()).count() > 8)
    			throw new UnsupportedOperationException("Teveel foto's op 1 pagina");
    		fotoSet.add(newFoto);
    	}
    }

    public int telPaginas() {
        //Opgave 1.6
    	if(fotoSet.size()==0)return 0;
        return (int) fotoSet.stream()
        		.map(e->e.getPaginaNummer())
        		.max((e1,e2)-> e1-e2).get();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Album van " + klantId);
        for (Foto foto : fotoSet) {
            sb.append("\n\tFoto ");
            sb.append(foto.toString());
        }
        sb.append("\n-------------------------------------------------------\n");
        sb.append("Pagina's: " + telPaginas());
        return sb.toString();
    }
    
    private class FotoComparator implements Comparator<Foto>{
		@Override
		public int compare(Foto arg0, Foto arg1) {
			return arg0.getNaam().compareTo(arg1.getNaam());
		}
    	
    }
}
