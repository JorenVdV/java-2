package fotoIO;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * IN DEZE KLASSE NIETS VERANDEREN!
 */
public final class FotoData {
    private static final Foto[] fotoReeks = {
            new Foto(1, new Dimension(640, 420), 20, 40),
            new Foto(1, new Dimension(1024, 768), 5, 520),
            new Foto(4, new Dimension(800, 600), 10, 10),
            new Foto(3, new Dimension(640, 480), 45, 67),
            new Foto(3, new Dimension(448, 336), 5, 5)
    };

    public static List<Foto> getFotoList() {
        List<Foto> fotoList = new ArrayList<>();
        for (Foto foto : fotoReeks) {
            fotoList.add(foto);
        }
        return fotoList;
    }
}
