package test;

import fotoIO.Foto;
import fotoIO.FotoAlbum;

import static org.junit.Assert.*;

import java.awt.Dimension;
import java.lang.System;
import org.junit.*;

/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 */
public class TestFotoAlbum {
    private FotoAlbum testAlbum;
    //Opgave 1.7
    @Before
    public void setUp(){
    	testAlbum = new FotoAlbum("oswald");
    }
    
    @Test (expected=UnsupportedOperationException.class)
    public void testToevoegen(){
    	for(int i=0; i<10; i++){
    		testAlbum.voegToe(new Foto(1, new Dimension(640, 420), 20, 40));
    	}
    	fail("9de pagina zou een UnsupportedOperationException moeten geven");
    }
    //Opgave 1.8
    @Test 
    public void testPaginas(){
    	assertEquals("Aantal pagina's in een leeg album moet 0 zijn",testAlbum.telPaginas(), 0);
    }
}
