/**
 * IN DEZE KLASSE NIETS VERANDEREN!
 */
public class Aandeel {
    private int id;
    private String naam;
    private double waarde;

    public Aandeel(int id, String naam, double waarde) {
        this.id = id;
        this.naam = naam;
        this.waarde = waarde;
    }

    @Reviewed("Mark")
    public int getId() {
        return id;
    }

    @Reviewed("Mark")
    public void setId(int id) {
        this.id = id;
    }

    @Reviewed("Kristiaan")
    public String getNaam() {
        return naam;
    }

    @Reviewed("Kristiaan")
    public void setNaam(String naam) {
        this.naam = naam;
    }

    public double getWaarde() {
        return waarde;
    }

    public void setWaarde(double waarde) {
        this.waarde = waarde;
    }

    @Reviewed("Karel")
    public void doeMaarWat(int id) {
        //nothing to do
    }

    @Reviewed("Kristiaan")
    public String toString() {
        return String.format("Aandeel: %4d %-20s %8.2f€", id, naam, waarde);
    }
}
