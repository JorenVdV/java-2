import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Examen OO programmeren 2 Java Datum: 09/01/2014
 */
public class Parser {
	public static void onderzoekCode(Class aClass) {
		Map<String, Integer> reviewMap = new TreeMap<>();
		// Opgave 2.2
		for(Method meth: aClass.getMethods()){
			if(meth.getAnnotation(Reviewed.class)!=null){
				String val = meth.getAnnotation(Reviewed.class).value();
				if(!reviewMap.containsKey(val))reviewMap.put(val, 1);
				else reviewMap.put(val, reviewMap.get(val)+1);
			}
		}
		System.out.println("Reviews in klasse " + aClass.getName() + ":");
		System.out.println(reviewMap);
	}
}
