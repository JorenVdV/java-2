import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Examen OO programmeren 2 Java Datum: 09/01/2014
 */
public class ListAttacker implements Runnable {
	private static List<Integer> getallenList = new ArrayList();
	private static Random random = new Random();
	private static final int AANTAL = 100;
	private Positie positie;
	private Bewerking bewerking;

	public ListAttacker(Positie positie, Bewerking bewerking) {
		this.positie = positie;
		this.bewerking = bewerking;
	}

	public static void maakList() {
		for (int i = 0; i < AANTAL; i++) {
			getallenList.add(random.nextInt());
		}
	}

	public static int getSize() {
		return getallenList.size();
	}

	@Override
	public void run() {
		for (int i = 0; i < AANTAL; i++) {
			synchronized (getallenList) {
				if(bewerking.equals(Bewerking.VERWIJDER)){
					while (getallenList.isEmpty()) {
						try {
							getallenList.wait();
						} catch (InterruptedException e) {
							//e.printStackTrace();
						}
					}
				}
				int pos = 0;
				switch (positie) {
				case VOORAAN:
					pos = 0;
					break;
				case MIDDEN:
					pos = getallenList.size() / 2;
					break;
				case ACHTERAAN:
					pos = getallenList.size() - 1;
				}
				try {
					switch (bewerking) {
					case VOEGTOE:
						getallenList.add(pos, random.nextInt());
						break;
					case VERWIJDER:
						getallenList.remove(pos);
					}
				} catch (Exception e) {
					System.out.println("Probleem: " + e);
				}
				getallenList.notify();
			}
			try {
				Thread.sleep(random.nextInt(3));
			} catch (InterruptedException e) {
				System.out.println(e);
			}
		}
		System.out.println(getClass().getName() + "_" + bewerking + "_" + positie + " be�indigd");
	}
}
