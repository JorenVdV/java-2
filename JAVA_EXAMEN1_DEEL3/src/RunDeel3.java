/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 */
public class RunDeel3 {
    private static final int AANTAL = 6;

    public static void main(String[] args) {
        Thread[] attackers = new Thread[AANTAL];
        int i = 0;
        for (Positie positie : Positie.values()) {
            for (Bewerking bewerking : Bewerking.values()) {
                attackers[i++] = new Thread(new ListAttacker(positie, bewerking));
            }
        }

        ListAttacker.maakList();
        System.out.println("Aantal elementen in de list voor attack: " + ListAttacker.getSize() + "\n");
        for (Thread thread : attackers) {
            thread.start();
        }
        //Opgave 3.1
        for(Thread thread: attackers){
        	try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        System.out.println("\nAantal elementen in de list na attack: " + ListAttacker.getSize());
    }
}
