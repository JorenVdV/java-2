//import fotoIO.Filiaal;
import fotoIO.Foto;
import fotoIO.FotoAlbum;
import fotoIO.FotoData;
import orderIO.Filiaal;
import orderIO.Order;
import orderIO.ToestandBesteld;

import java.util.List;

/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 */
public class RunDeel4 {
    public static void main(String[] args) {
        //Fotoalbum klaarmaken:
        List<Foto> fotoList = FotoData.getFotoList();
        FotoAlbum album = new FotoAlbum("Karel de Grote");
        for (Foto foto : fotoList) {
            album.voegToe(foto);
        }
        System.out.println("\nNa aanmaken fotoalbum:");
        System.out.println(album);

        //Fotoalbum bestellen:
        Order order = new ToestandBesteld(album); // Op het einde aanpassen: Order order = new ToestandBesteld(album);
        order.setBasisPrijs(0.75);
        System.out.println("\nNa aanmaken bestelling:");
        System.out.println(order);

        order = order.veranderToestand();
        order.setExtraKost(3.50);
        System.out.println("\nNa verwerking:");
        System.out.println(order);

        Filiaal filiaal = new Filiaal("Antwerpen Groenplaats");
        order.addObserver(filiaal);
        
        order = order.veranderToestand();
        order.setVerzendkost(5.00);
        System.out.println("\nNa verzending:");
        System.out.println(order);
    }
}