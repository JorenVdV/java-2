package fotoIO;

import java.awt.*;

/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 * NAAM:
 * KLASGROEP:
 */

// final for blocking subclasses
public final class Foto {
    private static int volgNummer;
    private final String naam;
    private int paginaNummer;
    private Dimension dimension;
    private int x;
    private int y;

    public Foto(int paginaNummer, Dimension dimension, int x, int y) {
        this.paginaNummer = paginaNummer;
        this.dimension = dimension;
        this.x = x;
        this.y = y;
        //Opgave 1.2
        this.naam = String.format("%03d_%02d",paginaNummer, volgNummer++);
    }

    public int getPaginaNummer() {
        return paginaNummer;
    }

    public String getNaam() {
        return naam;
    }

    @Override
    public String toString() {
        //Opgave 1.3
        return String.format("Foto %s   p%2d   pos:%4d,%-4d   size: %dx%d", 
        		naam,paginaNummer, x, y, dimension.width, dimension.height);
    }
    
    @Override
    public boolean equals(Object other){
    	if(Object.class.equals(this.getClass())){
    		Foto otherthis = (Foto) other;
    		return this.naam.equals(otherthis.naam);
    	}
    	return false;
    }
}
