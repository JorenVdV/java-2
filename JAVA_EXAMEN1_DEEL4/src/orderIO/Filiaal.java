package orderIO;

import java.util.Observable;
import java.util.Observer;

import fotoIO.FotoAlbum;

/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 */
public class Filiaal implements Observer{
    private String naam;

    public Filiaal(String naam) {
        this.naam = naam;
    }

	@Override
	public void update(Observable o, Object arg) {
		//System.err.println("rekt");
		if(arg!=null){
		if(arg.getClass().equals(FotoAlbum.class)){
			FotoAlbum al = (FotoAlbum) arg;
			System.out.printf("Klant %s wordt verwittigd dat zijn bestelling klaar lig in filiaal %s.\n", al.getKlantId(), naam);
		}}
		else System.err.println("help");
		//*/
		
	}
}
