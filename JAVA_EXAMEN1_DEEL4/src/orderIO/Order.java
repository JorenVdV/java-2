package orderIO;

import java.util.Observable;
import java.util.Observer;

import fotoIO.FotoAlbum;

/**
 * Examen OO programmeren 2 Java
 * Datum: 09/01/2014
 */
public abstract class Order extends Observable{

    protected FotoAlbum album;
    protected double basisPrijs;
    protected double extraKost;
    protected double verzendkost;

    public Order(FotoAlbum album) {
        this.album = album;
        basisPrijs = 0.65; //by default
        extraKost = 3.80; //by default
        verzendkost = 5.55; //by default
    }

    public Order(Order other) {
        this.album = other.album;
        this.basisPrijs = other.basisPrijs;
        this.extraKost = other.extraKost;
        this.verzendkost = other.verzendkost;
    }

    @Override
    public String toString() {
        return album.toString() + String.format("\nPrijs   : %.2f$", berekenPrijs());
    }

    public abstract double berekenPrijs();
    public abstract Order veranderToestand();
    public void setBasisPrijs(double basisPrijs){}
    public void setExtraKost(double extraKost){}
    public void setVerzendkost(double verzendkost){}

    
    
}

