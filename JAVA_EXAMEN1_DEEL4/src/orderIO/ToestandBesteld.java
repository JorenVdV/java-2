package orderIO;

import fotoIO.FotoAlbum;

public class ToestandBesteld extends Order {

	public ToestandBesteld(FotoAlbum album) {
		super(album);
	}

    public void setBasisPrijs(double basisPrijs) {
    	this.basisPrijs = basisPrijs;
    }
	
	@Override
	public double berekenPrijs() {
		return basisPrijs * album.telPaginas();
	}
	

	@Override
	public Order veranderToestand() {
		return new ToestandVerwerkt(this);
	}

}
