package orderIO;

public class ToestandVerwerkt extends Order {

	public ToestandVerwerkt(Order other) {
		super(other);
	}

	@Override
	public double berekenPrijs() {
		return basisPrijs * album.telPaginas() + extraKost;
	}

	@Override
	public Order veranderToestand() {
		setChanged();
		notifyObservers(album);
		return new ToestandVerzonden(this);
	}

	public void setExtraKost(double extraKost) {
		this.extraKost = extraKost;
	}

}
