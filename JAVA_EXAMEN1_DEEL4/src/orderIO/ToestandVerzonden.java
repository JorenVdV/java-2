package orderIO;

public class ToestandVerzonden extends Order {

	public ToestandVerzonden(Order other) {
		super(other);
	}

	@Override
	public double berekenPrijs() {
		return basisPrijs * album.telPaginas() + extraKost + verzendkost;
	}

	@Override
	public Order veranderToestand() {
		return this;
	}

	public void setVerzendkost(double verzendkost) {
		this.verzendkost = verzendkost;
	}

}
