/*
    Pas aan:
    De klasse Reeks implementeert de interface Deelbaar
 */
public class Reeks implements Deelbaar{
    private int[] array;

    public Reeks(int[] array){
        this.array = array;
    }

    public String toString() {
        String str = "[";
        for(int i = 0; i < array.length; i++) {
            str += array[i];
            if(i < array.length - 1) str += ", ";
        }
        return str + "]";
    }

	@Override
	public Deelbaar getHelft() {
		int[] array2 = new int[array.length/2];
		System.arraycopy(array, 0, array2, 0,array2.length);
		return new Reeks (array2);
	}
}
