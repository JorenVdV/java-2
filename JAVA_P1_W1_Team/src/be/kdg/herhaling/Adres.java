package be.kdg.herhaling;

public class Adres {
    private String straat;
    private int postNummer;
    private String gemeente;

    // 1.1 Maak de constructor + getters
    public Adres(String straat, int postNummer, String gemeente){
    	this.straat= straat;
    	this.postNummer=postNummer;
    	this.gemeente=gemeente;
    }
    
    public String getStraat(){
    	return this.straat;
    }
    
    public int getPostNummer(){
    	return this.postNummer;
    }
    
    public String getGemeente(){
    	return this.gemeente;
    }

    // 1.2 Override de toString methode
    public String toString() {
        return String.format("%20s %d %s", straat, postNummer, gemeente);
    }
}
