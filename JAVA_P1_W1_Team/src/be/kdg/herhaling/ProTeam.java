package be.kdg.herhaling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ProTeam extends Team{
    private String sponsorNaam;
    private SponsorSoort sponsorSoort;

    // 6.1 Laat deze klasse van de klasse Team overerven

    // 6.2 Schrijf de nodige constructor + getters voor beide attributen
    public ProTeam(String naam, String sponsorNaam, SponsorSoort sponsorSoort){
    	super(naam);
    	this.sponsorNaam=sponsorNaam;
    	this.sponsorSoort=sponsorSoort;
    }
    
    public String getSponsorNaam(){
    	return this.sponsorNaam;
    }
    
    public SponsorSoort getSponsorSoort(){
    	return this.sponsorSoort;
    }
    
    // 6.3 Schrijf een getter voor de naam van het team
    public String getTeamNaam(){
    	return super.getNaam();
    }
    
    // 6.4 Implementeer hier de methode van de interface (showTeam),
    // ze toont alle informatie in verband met het team (zie verder voor een voorbeeld)
    public void showTeam() {
    	System.out.println(String.format("Naam: %s", super.getNaam()));
    	System.out.println(String.format("Sponsor: %s (%s)", sponsorNaam, sponsorSoort.toString()));
    	System.out.println("Leden: ");
    	super.getSpelers().stream().forEach(System.out::println);
    }

    // 6.5  Vul de methode verhaspelSponsorNaam aan.
    // De methode dient de volgorde van de letters van de sponsornaam willekeurig door elkaar te halen
    // en als String terug te geven.
    public String verhaspelSponsorNaam() {
        List<String> list = Arrays.asList(sponsorNaam.split(""));
        Collections.shuffle(list);
        return list.stream().collect(Collectors.joining(""));
    }
}
