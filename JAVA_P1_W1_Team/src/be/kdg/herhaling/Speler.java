package be.kdg.herhaling;

import java.util.Comparator;


public class Speler {
    private int rugNummer;
    private String naam;
    private Adres adres;

    // 2.1 Maak beide constructors + een getter voor het rugNummer
    public Speler(int rugNummer, String naam, Adres adres){
    	this.rugNummer= rugNummer;
    	this.naam = naam;
    	this.adres=adres;
    }
    
    public Speler(int rugNummer, String naam, String straat, int postNummer, String gemeente){
    	this(rugNummer, naam, new Adres(straat, postNummer, gemeente));
    	// use this instead of constructor name!!
    }
    
    public int getRugNummer(){
    	return this.rugNummer;
    }
    // 2.2 Override de toString methode (zie gewenste uitvoer - werk met kolommen)
    public String toString() {
        return String.format("%2d %-20s %s", rugNummer, naam, adres.toString());
    }
    
    public static class Comparators {
        public static final Comparator<Speler> NAME = (Speler s1, Speler s2) -> s1.naam.compareTo(s2.naam);
    }

}
