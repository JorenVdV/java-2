import java.util.Arrays;

/**
 * De opgaves staan in de code. Implementeer eerst Product, dan Winkellijst en dan WinkellijstUI
 */
public class Product implements Comparable<Product> {
    static final String[] TYPES = {"Drank", "Fruit", "Groenten", "Zuivel", "Non-food", "Diepvries", "Baby", "Bakkerij", "Overige"};

    private String naam;
    private String type;

    /**
     * Initialisatie van de attributen via de constructor
     * Controleer of de naam niet leeg is en of het type uit de lijst TYPES komt, zoniet: IllegalArgumentException throwen!
     * @throws Exception 
     */
    public Product(String naam, String type) throws Exception {
    	if (!Arrays.asList(TYPES).contains(type)) throw new IllegalArgumentException("Onbekend type!");
        this.naam=naam;
        this.type=type;
    }

    /**
     * Creeer getters voor beide attributen
     */
	public String getNaam() {
		return naam;
	}

	public String getType() {
		return type;
	}

    /**
     * Creeer toString: geeft "type:    naam" (bv: "Fruit:  bananen")
     */
	public String toString(){
		return String.format("%-15s %s", type+":", naam);
	}
    /**
     * Implementeer: sorteer eerst op type daarna op naam
     */
    public int compareTo(Product product) {
    	if(type.equals(product.type))return naam.compareTo(product.naam);
        return type.compareTo(product.type);
    }


}
