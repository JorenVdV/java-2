import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Winkellijst bevat 1 attribuut: een ArrayList van producten
 */
public class Winkellijst {
	List<Product> lijst;
    /**
     * Initialiseer het attribuut
     */
    public Winkellijst() {
    	lijst = new ArrayList<>();
    }

    /**
     * Voeg product toe. Sorteer ook telkens de lijst.
     * @param product Het product
     */
    public void addProduct(Product product) {
        lijst.add(product);
        Collections.sort(lijst);
    }

    /**
     * Maak de lijst leeg
     */
    public void clear() {
        lijst.clear();
    }

    /**
     * toString: returns een String die alle producten achter elkaar bevat (met newlines ertussen)
     */
    public String toString() {
        return lijst.stream().map(Product::toString).collect(Collectors.joining("\n"));
    }

    /**
     * Deze main kan je gebruiken om te testen wat je tot nu toe hebt. Verwachte output staat onderaan
     * @param args Niet gebruikt
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        Winkellijst lijst = new Winkellijst();
        Product p = new Product("Appels", "Fruit");
        lijst.addProduct(p);
        p = new Product("Peren", "Fruit");
        lijst.addProduct(p);
        try {
            new Product("Cola", "Drankje");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        p = new Product("Pizza", "Diepvries");
        lijst.addProduct(p);
        System.out.println("Winkellijst:\n" + lijst);
        lijst.clear();
        System.out.println("Winkellijst:\n" + lijst);
    }
}

/* Verwachte output:
Onbekend type...!
Winkellijst:
Diepvries:	Pizza
Fruit:	Appels
Fruit:	Peren

Winkellijst:

*/
