import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class WinkellijstUI extends JFrame {
    private JTextField tfItem;
    private JComboBox cbType;
    private JTextArea taWinkellijst;
    private JButton btnSave;
    private JButton btnClear;
    private JButton btnAdd;

    private Winkellijst lijst;

    /**
     * De constructor hoeft niet meer aangepast te worden
     */
    public WinkellijstUI() {
        super("Winkellijst");
        lijst = new Winkellijst();
        createComponenten();
        maakLayout();
        addListeners();
        setSize(600, 500);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * Maak de verschillende componenten aan.
     * cbType bevat alle TYPES uit Product.
     * taWinkellijst is niet editeerbaar
     */
    private void createComponenten() {
        //
    }

    /**
     * Probeer zo goed mogelijk de layout uit de screenshot na te bootsen... (zie screenshot.rtf).
     * De layout hoeft niet perfect hetzelfde te zijn. Zorg in alle geval dat al de componenten erop staan...
     */
    private void maakLayout() {
        //
    }

    /**
     * Voeg je actionListeners toe:
     * btnAdd: voeg nieuw product aan lijst: gebruik private method addProduct() (Zie onderaan)
     * btnClear: clear de lijst en de textarea
     * btnSave: save de lijst naar het filesysteem: gebruik private method saveWinkellijst() (zie onderaan)
     */
    private void addListeners() {
        //
    }

    /**
     * Bewaar de winkellijst op het filesysteem, gebruik makend van JFileChooser.
     * Toon een JOptionpane.INFORMATION_MESSAGE als het gelukt is
     * Toon een JOptionPane.ERROR_MESSAGE als er een IOException ontstaat
     */
    private void saveWinkellijst() {
        JFileChooser fc = new JFileChooser();
        fc.showSaveDialog(this);
        File file = fc.getSelectedFile();

    }

    /**
     * Creeer nieuw Product ahv de naam en het type uit tfItem en cbType.
     * Voeg het toe aan de lijst. Update taWinkellijst
     * Maak tfItem terug leeg en geef het de focus.
     */
    private void addProduct() {
        //
    }
}
