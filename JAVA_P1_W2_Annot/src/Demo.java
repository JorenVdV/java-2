import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class Demo {
	public void demoMethode() {

		MijnKlasse mijnKlasse = new MijnKlasse();
		// Toon alle annotaties voor de klasse MijnKlasse.
		toonClassAnnotations(mijnKlasse);
		// Toon de waarden van de Class-annotaties
		toonWaardenClassAnnotation(mijnKlasse);
		// Toon alle annotaties voor demoMethode uit MijnKlasse .
		toonMethodeAnnotations(mijnKlasse);
		// Toon alle waarden van de Methode-annotaties uit MijnKlasse
		toonWaardenMethodeAnnotation(mijnKlasse);
	}

	private void toonWaardenMethodeAnnotation(MijnKlasse mijnKlasse) {
		System.out.println("Alle parameters:");
		try {
			Method method = mijnKlasse.getClass().getMethod("demoMethode");
			System.out.print(method.getAnnotation(MijnAnnotatie.class).string());
			System.out.println(method.getAnnotation(MijnAnnotatie.class).waarde());
			System.out.println(method.getAnnotation(Wat.class).omschrijving());
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	private void toonMethodeAnnotations(MijnKlasse mijnKlasse) {
		System.out.println("Alle annotaties voor demoMethode:");
		try {
			Method method = mijnKlasse.getClass().getMethod("demoMethode");
			for (Annotation annot : method.getAnnotations())
				System.out.println(annot.toString());
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		System.out.println();

	}

	private void toonWaardenClassAnnotation(MijnKlasse mijnKlasse) {
		System.out.println("Alle parameters:");
		System.out.print(mijnKlasse.getClass().getAnnotation(MijnAnnotatie.class).string());
		System.out.println(mijnKlasse.getClass().getAnnotation(MijnAnnotatie.class).waarde());
		System.out.println(mijnKlasse.getClass().getAnnotation(Wat.class).omschrijving());
		System.out.println();

	}

	private void toonClassAnnotations(MijnKlasse mijnKlasse) {
		System.out.println("Alle annotaties voor MijnKlasse:");
		for (Annotation annot : mijnKlasse.getClass().getAnnotations())
			System.out.println(annot.toString());
		System.out.println();

	}
}