import bankaccount.BankAccount;
import bankaccount.BankAccountValidator;

/**
 * Hier moet je niets wijzigen!

 * 1) Schrijf een testklasse TestBankAccount om de toString-methode van de
 *    klasse BankAccount te testen.
 *
 * 2) Schrijf een testklasse TestBankAccountValidator om de validatie
 *    van het rekeningnummer te testen.
 */
public final class Demo {
    public static void main(String[] args) {
        String[] accountNumbers = {
                "409407376196", "730004200601",
                "12345678901", "73o004200601", "123456789012"};

        for (String accountNumber : accountNumbers) {
            try {
                // validateAccount may throw an IllegalArgumentException (is unchecked exception)
                BankAccountValidator.validateAccount(accountNumber);
                BankAccount ba = new BankAccount(accountNumber);
                System.out.println(accountNumber + " accepted: " + ba);
            } catch (IllegalArgumentException e) {
                System.out.println(accountNumber + " NOT accepted: " + e);
            }
        }
    }
}

/* Verwachte uitvoer:
409407376196 accepted: 409-4073761-96
730004200601 accepted: 730-0042006-01
12345678901 NOT accepted: bankaccount.IllegalArgumentException: BankAccount must have 12 digits
73o004200601  NOT accepted: bankaccount.IllegalArgumentException: Wrong string, must be numeric
123456789012 NOT accepted: bankaccount.IllegalArgumentException: Wrong accountnumber
*/
