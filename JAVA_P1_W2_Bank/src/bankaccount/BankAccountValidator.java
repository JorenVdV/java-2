/**
 * Hier moet je niets wijzigen!
 */
package bankaccount;

public final class BankAccountValidator {
    public static void validateAccount(String account) throws IllegalArgumentException {
        if (account.length() != 12) {
            throw new IllegalArgumentException("BankAccount must have 12 digits");
        }
        try {
            Long.parseLong(account);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Wrong string, must be numeric");
        }
        if (!isValidNumber(account)) {
            throw new IllegalArgumentException("Wrong accountnumber");
        }
    }

    private static boolean isValidNumber(String account) throws NumberFormatException {
        long eigenlijkNummer = Long.parseLong(account.substring(0, 10));
        int controleGetal = Integer.parseInt(account.substring(10, 12));
        if (eigenlijkNummer % 97 == 0) {
            return controleGetal == 97;
        }
        return (eigenlijkNummer % 97) == controleGetal;
    }

}
