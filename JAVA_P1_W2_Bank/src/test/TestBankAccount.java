/**
 * Opdracht: Werk de klasse TestBankAccount uit.
 * Werk de testmethode uit zodat de test groen licht geeft.
 */

package test;

import junit.framework.JUnit4TestAdapter;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import bankaccount.BankAccount;

/**
 * Testklasse voor JUnit, test de klasse BankAccount.
 */
public class TestBankAccount {

    /**
     * Test de constructor en de toString methode.
     */
    @Test
    public void testToString() {
        BankAccount account = new BankAccount("409407376196");
        // vul aan
        assertEquals("",account.toString(),"409-4073761-96");
    }

}

