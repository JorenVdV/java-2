/**
 * Opdracht: Werk de klasse TestBankAccountValidator uit.
 * Werk alle testmethoden uit zodat alle testen groen licht geven.
 */

package test;

import static org.junit.Assert.fail;
import org.junit.Test;

/**
 * Testklasse voor JUnit, test de klasse BankAccountValidator.
 */
public class TestBankAccountValidator {
    /** Testgegevens */
    private String[] accounts = {
            "409407376196", "730004200601", "001501404897", // juiste nummers!
            "12345678901", "73o004200601",
            "123456789012", "001501404800"};

    /**
     * Test de validatie van twee goede rekeningnummers.
     * Er mag geen exception optreden.
     */
    @Test
    public void testValidateAccount() {

    }

    /**
     * Test de validatie van een rekeningnummer met te weinig cijfers.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test
    public void testValidateAccountWithWrongLength() {

    }

    /**
     * Test de validatie van een rekeningnummer dat niet numerieke tekens bevat.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test
    public void testValidateAccountWithWrongChar() {

    }

    /**
     * Test de validatie van een niet geldig rekeningnummer.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test
    public void testValidateAccountWithWrongNumber() {

    }

    /**
     * Test de validatie van een niet geldig rekeningnummer.
     * Rest is 0 --> controlegetal moet dan 97 zijn.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test
    public void testValidateAccountWithWrongNumberRestDoubleZero() {

    }
}