import java.lang.reflect.*;
import java.lang.annotation.Annotation;

public class ParserClass {
    public static void execute(Class<?> aClass) {
        try {
            Object anObject = aClass.newInstance();

            for (Method method : aClass.getDeclaredMethods()) {
                Kleur kleurAnnot = method.getAnnotation(Kleur.class);
                if (kleurAnnot != null) {
                	aClass.getDeclaredMethod("setKleur", String.class).invoke(anObject, kleurAnnot.kleurNaam());
                    method.invoke(anObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}


