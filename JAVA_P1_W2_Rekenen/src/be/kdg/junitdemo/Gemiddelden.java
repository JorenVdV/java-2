package be.kdg.junitdemo;

import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * In deze klasse vind je een aantal logische fouten terug.
 * Verbeter ze aan de hand van de bijgevoegde testklasse TestGemiddelden.
 * Zorg ervoor dat je voor alle testen een groene balk krijgt!
 */
public class Gemiddelden {
    private ArrayList<Double> getallen;
    private double som = 0;
    private double product = 0;

    public Gemiddelden() {
        this.getallen = new ArrayList<Double>();
    }

    public void voegGetalToe(double getal) {
        getallen.add(getal);
    }

    public void maakLeeg() {
        this.getallen.clear();
    }

    // Klassieke berekening van het gemiddelde
    public double rekenkundigGemiddelde() {
    	som=0;
    	if(getallen.size()==0){
    		throw new ArithmeticException();
    	}
        for(Double getal : getallen) {
            som += getal;
        }
        return som / getallen.size();
    }

    // Zie onder ander http://nl.wikipedia.org/wiki/Meetkundig_gemiddelde
    // en uiteraard Javadoc voor de pow-methode.
    public double meetkundigGemiddelde() {
    	if(getallen.size()==0)throw new ArithmeticException();
    	product=1;
        for(Double getal : getallen) {
            product *= getal;
        }
        return Math.pow(product, 1 / (double)getallen.size());
    }
}
