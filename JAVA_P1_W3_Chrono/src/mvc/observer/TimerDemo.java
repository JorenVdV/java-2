package mvc.observer;

import mvc.observer.model.ChronoMeter;
import mvc.observer.view.TimerFrame;

/*
 Opdracht:

 1. Voorzie het nodige om de chronometer te laten werken (programma compileert reeds).
    Je moet gebruik maken van het Observer pattern!

 2. Vul het model aan met Javadoc. Voorzie ook elke klasse bovenaan van een commentaar (waarvoor de klasse dient)

 3. Genereer de Javadoc in een doc directory die je in de root van je project maakt.
    Zie het bestand doc.zip voor een voorbeeld van wat we verwachten.
 */

public class TimerDemo {
    public static void main(String[] args) {
        new TimerFrame(new ChronoMeter());
    }
}
