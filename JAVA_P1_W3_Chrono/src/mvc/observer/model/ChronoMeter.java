package mvc.observer.model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;


public class ChronoMeter extends Observable{
    private int minuten = 0;
    private int seconden = 0;
    private Timer timer;

    /**
     * De constructor stelt een timer in die elke seconde een event genereerd.
     * De timer wordt ingesteld op 1000 milliseconden en er wordt een ActionListener aan gekoppeld.
     * De methode increment verhoogd de tijd met 1 seconde.
     *
     * <pre>
     *    timer = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                increment();
            }
          });
     * </pre>
     */
    public ChronoMeter() {
        timer = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                increment();
            }
        });
    }

    private void increment() {
        seconden++;
        if (seconden >= 60) {
            seconden = 0;
            minuten++;
            if (minuten >= 60) {
                minuten = 0;
            }
        }
        setChanged();
        notifyObservers();
    }

    public String getMinutenString() {
        String minutenString = Integer.toString(this.minuten);
        if (this.minuten < 10) {
            return "0" + minuten;
        }
        return minutenString;
    }

    public String getSecondenString() {
        String secondenString = Integer.toString(seconden);
        if (seconden < 10) {
            return "0" + seconden;
        }
        return secondenString;
    }

    public void reset() {
        timer.stop();
        minuten = 0;
        seconden = 0;
        setChanged();
        notifyObservers();
    }


    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
        setChanged();
        notifyObservers();
    }
}