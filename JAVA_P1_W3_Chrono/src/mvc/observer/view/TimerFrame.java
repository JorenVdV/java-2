package mvc.observer.view;

import mvc.observer.model.ChronoMeter;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class TimerFrame extends JFrame implements Observer{
    private ChronoMeter chrono;

    private JPanel displayPanel;

    private JTextField minutenField;
    private JTextField secondenField;
    private JButton startButton;
    private JButton stopButton;
    private JButton resetButton;
    private JRadioButton showButton;
    private JRadioButton hideButton;

    public TimerFrame(ChronoMeter chrono) {
        super("ChronoDemo");
        this.chrono = chrono;
        chrono.addObserver(this);


        maakDisplayPanel();
        maakControlPanel();
        voegListenersToe();
        toonFrame();
    }

    private void maakDisplayPanel() {
        JPanel timePanel = new JPanel(new GridLayout(1, 0, 3, 3));
        Font font = new Font("Helvetica", Font.PLAIN, 60);
        minutenField = new JTextField("00");
        minutenField.setFont(font);
        minutenField.setBackground(Color.white);
        secondenField = new JTextField("00");
        secondenField.setFont(font);
        secondenField.setBackground(Color.white);
        minutenField.setEditable(false);
        secondenField.setEditable(false);
        JLabel label = new JLabel(":", JLabel.CENTER);
        label.setFont(font);

        timePanel.add(minutenField);
        timePanel.add(label);
        timePanel.add(secondenField);

        displayPanel = new JPanel();
        displayPanel.add(timePanel);

        add(displayPanel, BorderLayout.NORTH);
    }

    private void maakControlPanel() {
        showButton = new JRadioButton("show", true);
        hideButton = new JRadioButton("hide", false);
        ButtonGroup group = new ButtonGroup();
        group.add(showButton);
        group.add(hideButton);

        JPanel showPanel = new JPanel(new GridLayout(0, 1));
        showPanel.add(showButton);
        showPanel.add(hideButton);
        showPanel.setBackground(Color.GRAY);

        JPanel buttonPanel = new JPanel(new GridLayout(1, 0, 3, 3));
        startButton = new JButton("Start");
        stopButton = new JButton("Stop");
        resetButton = new JButton("Reset");
        startButton.setEnabled(true);
        stopButton.setEnabled(false);
        resetButton.setEnabled(false);

        buttonPanel.add(startButton);
        buttonPanel.add(stopButton);
        buttonPanel.add(resetButton);
        buttonPanel.add(showPanel);
        buttonPanel.setBackground(Color.GRAY);

        JPanel controlPanel = new JPanel();
        controlPanel.add(buttonPanel);
        controlPanel.setBackground(Color.GRAY);
        
        add(controlPanel, BorderLayout.SOUTH);
    }

    private void voegListenersToe() {
        startButton.addActionListener(new MyListener());
        stopButton.addActionListener(new MyListener());
        resetButton.addActionListener(new MyListener());
        showButton.addActionListener(new MyListener());
        hideButton.addActionListener(new MyListener());
    }

    private void toonFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        pack();
        setLocationRelativeTo(null);

        startButton.requestFocus();
    }

    private class MyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == startButton) {
                chrono.start();
                startButton.setEnabled(false);
                stopButton.setEnabled(true);
                resetButton.setEnabled(false);
            } else if (e.getSource() == stopButton) {
                chrono.stop();
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
                resetButton.setEnabled(true);
            } else if (e.getSource() == resetButton) {
                chrono.reset();
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
                resetButton.setEnabled(false);
            } else if (e.getSource() == showButton) {
                displayPanel.setVisible(true);
            } else if (e.getSource() == hideButton) {
                displayPanel.setVisible(false);
            }
        }
    }

	@Override
	public void update(Observable o, Object arg) {
		if(!o.getClass().equals(ChronoMeter.class))return;
		ChronoMeter cm = (ChronoMeter)o;
		minutenField.setText(cm.getMinutenString());
		secondenField.setText(cm.getSecondenString());
	}
}
