package rekenen;

import rekenen.data.Tabel;
import rekenen.frames.TabelFrame;

public class RunDemo {
    public static void main(String[] args) {
        Tabel tabel = new Tabel(10);
        new TabelFrame(tabel);
    }
}
