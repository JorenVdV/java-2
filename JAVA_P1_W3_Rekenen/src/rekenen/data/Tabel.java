package rekenen.data;

import java.util.Observable;

public class Tabel extends Observable{
    private int[] tabel;       // tabel van gehele getallen
    private boolean[] gevuld;  // welke getallen in de tabel zijn ingevuld?
    private int grootte;       // lengte van de tabel

    /**
     * Constructor voor een tabel met "grootte" aantal elementen.
     *
     * @param grootte Aantal elementen in de tabel
     */
    public Tabel(int grootte) {
        this.grootte = grootte;
        tabel = new int[grootte];
        gevuld = new boolean[grootte];
    }

    /**
     * Vult een getalwaarde (waarde) op een gegeven plaats (index) in de tabel.
     *
     * @param index  Plaats in de tabel
     * @param waarde In te vullen waarde
     */
    public void setWaarde(int index, int waarde) {
        if (index >= 0 && index < grootte) {
            tabel[index] = waarde;
            gevuld[index] = true;
        }
        setChanged();
        notifyObservers();
    }

    /**
     * Geeft de lengte van de tabel.
     *
     * @return Lengte van de tabel
     */
    public int grootte() {
        return grootte;
    }

    /**
     * Geeft het aantal ingevulde getalwaarden.
     *
     * @return Aantal ingevulde getallen
     */
    public int aantal() {
        int aantal = 0;
        for (int i = 0; i < grootte; i++) {
            if (gevuld[i]) {
                aantal++;
            }
        }
        return aantal;
    }

    /**
     * Geeft de som van de ingevulde getalwaarden.
     *
     * @return Som van de in gevulde getallen
     */
    public int som() {
        int som = 0;
        for (int i = 0; i < grootte; i++) {
            if (gevuld[i]) {
                som += tabel[i];
            }
        }
        return som;
    }

    /**
     * Geeft het rekenkundig gemiddelde van de ingevulde getalwaarden.
     *
     * @return Gemiddelde van de getallen
     */
    public double gemiddelde() {
        return (double) som() / aantal();
    }

    /**
     * Maakt de tabel volledig leeg.
     */
    public void reset() {
        for (int i = 0; i < tabel.length; i++) {
            tabel[i] = 0;
            gevuld[i] = false;
        }
        aantal();
        som();
        setChanged();
        notifyObservers();
    }
}

