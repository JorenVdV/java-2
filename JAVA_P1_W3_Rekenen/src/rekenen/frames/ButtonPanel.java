package rekenen.frames;

import rekenen.data.Tabel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ButtonPanel extends JPanel {
	private Tabel tabel;
	InvoerPanel invoerPanel;
	JButton okButton;
    JButton resetButton;
	
    public ButtonPanel(Tabel tabel, InvoerPanel invoerPanel) {
    	this.tabel = tabel;
    	this.invoerPanel = invoerPanel;
        okButton = new JButton("Ok");
        resetButton = new JButton("Reset");

        JPanel hulpPanel = new JPanel(new GridLayout(1, 0, 5, 5));
        hulpPanel.add(okButton);
        hulpPanel.add(resetButton);
        add(hulpPanel);

        setBackground(new Color(255, 200, 150));
        
        AddListeners();
    }
    
    private void AddListeners(){
    	okButton.addActionListener(new MyListener());
    	resetButton.addActionListener(new MyListener());
    }
    
    private class MyListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == okButton) {
				tabel.reset();
				int index =-1;
				for(JTextField field : invoerPanel.fields){
					index++;
					if(field.getText().equals(""))continue;
					tabel.setWaarde(index, Integer.parseInt(field.getText()));
				}
				
			} else if (e.getSource() == resetButton) {
				tabel.reset();
			}
			
		}
    	
    }
}
