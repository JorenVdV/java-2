package rekenen.frames;

import rekenen.data.Tabel;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

class InvoerPanel extends JPanel implements Observer{
    JTextField[] fields;  // package toegankelijkheid!

    public InvoerPanel(Tabel tabel) {
    	tabel.addObserver(this);
        int aantal = tabel.grootte();
        setBackground(new Color(255, 150, 200));
        fields = new JTextField[aantal];
        JLabel[] labels = new JLabel[aantal];

        JPanel hulpPanel = new JPanel(new GridLayout(aantal, 0, 5, 5));
        for (int i = 0; i < aantal; i++) {
            labels[i] = new JLabel();
            labels[i].setText("Getal " + (i + 1) + " :");
            fields[i] = new JTextField(3);

            hulpPanel.add(labels[i]);
            hulpPanel.add(fields[i]);
            hulpPanel.setBackground(new Color(255, 150, 200));
        }
        add(hulpPanel);
    }

	@Override
	public void update(Observable o, Object arg) {
		Tabel tab = (Tabel) o;
		for (int i=0; i < tab.grootte(); i++){
			fields[i] = new JTextField(3);
		}
		
	}
}
