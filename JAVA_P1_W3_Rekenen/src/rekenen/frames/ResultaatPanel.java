package rekenen.frames;

import rekenen.data.Tabel;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

class ResultaatPanel extends JPanel implements Observer{
    private JTextField aantalField;
    private JTextField somField;
    private JTextField gemiddeldeField;
    private Tabel tabel;

    public ResultaatPanel(Tabel tabel) {
    	this.tabel = tabel;
    	tabel.addObserver(this);
    	
        maakComponenten();
        maakLayout();
        setBackground(new Color(150, 200, 255));
    }

    private void maakLayout() {
        JPanel hulpPanel = new JPanel(new GridLayout(0, 1, 5, 5));
        hulpPanel.add(new JLabel("Aantal"));
        hulpPanel.add(aantalField);
        hulpPanel.add(new JLabel("Som"));
        hulpPanel.add(somField);
        hulpPanel.add(new JLabel("Gemiddelde"));
        hulpPanel.add(gemiddeldeField);
        hulpPanel.setBackground(new Color(150, 200, 255));

        add(hulpPanel);
    }

    private void maakComponenten() {
        aantalField = new JTextField(3);
        somField = new JTextField(6);
        gemiddeldeField = new JTextField(10);

        aantalField.setEditable(false);
        somField.setEditable(false);
        gemiddeldeField.setEditable(false);
    }

	@Override
	public void update(Observable o, Object arg) {
		aantalField.setText(Integer.toString(tabel.aantal()));
		somField.setText(Integer.toString(tabel.som()));
		gemiddeldeField.setText(Double.toString(tabel.gemiddelde()));
	}
}
