package rekenen.frames;

import rekenen.data.Tabel;

import javax.swing.*;
import java.awt.*;

public class TabelFrame extends JFrame {
    private InvoerPanel invoerPanel;
    private ResultaatPanel resultaatPanel;
    private ButtonPanel buttonPanel;

    public TabelFrame(Tabel tabel) {
        setTitle("DemoFrame");

        maakComponenten(tabel);
        maakLayout();
        toonFrame();
    }

    private void maakLayout() {
        add(invoerPanel, BorderLayout.WEST);
        add(resultaatPanel, BorderLayout.EAST);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void maakComponenten(Tabel tabel) {
        invoerPanel = new InvoerPanel(tabel);
        resultaatPanel = new ResultaatPanel(tabel);
        buttonPanel = new ButtonPanel(tabel, invoerPanel);
    }

    private void toonFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension d = getToolkit().getScreenSize();
        setLocation(d.width / 3, d.height / 3);
        pack();
        setVisible(true);
    }
   
}
