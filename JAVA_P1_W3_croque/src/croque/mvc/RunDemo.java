package croque.mvc;

import croque.mvc.model.Croque;
import croque.mvc.view.CroqueFrame;

public class RunDemo {
    public static void main(String[] args) {
        Croque croque = new Croque();
        new CroqueFrame(croque);
    }
}
