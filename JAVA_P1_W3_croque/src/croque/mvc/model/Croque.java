package croque.mvc.model;

public class Croque {
    private static final int BASIS_PRIJS = 200;
    private static final int KAAS_PRIJS = 50;
    private static final int HAM_PRIJS = 60;
    private static final int ANANAS_PRIJS = 30;

    private boolean metKaas = false;
    private boolean metHam = false;
    private boolean metAnanas = false;

    // De statements in commentaar hebben hier voorlopig nog geen zin, wel bij de Observer versie!

    /*
    private String betaalWijze = "Contant";

    public String getBetaalWijze() {
        return betaalWijze;
    }

    public void setBetaalWijze(String wijze) {
        betaalWijze = wijze;
    }
    */

    public void setMetKaas(boolean metKaas) {
        this.metKaas = metKaas;
    }

    public void setMetHam(boolean metHam) {
        this.metHam = metHam;
    }

    public void setMetAnanas(boolean metAnanas) {
        this.metAnanas = metAnanas;
    }

    public String teBetalen() {
        int teBetalen = BASIS_PRIJS;

        if (metKaas) {
            teBetalen += KAAS_PRIJS;
        }
        if (metHam) {
            teBetalen += HAM_PRIJS;
        }
        if (metAnanas) {
            teBetalen += ANANAS_PRIJS;
        }
        return Double.toString((double) teBetalen / 100);
    }
}
