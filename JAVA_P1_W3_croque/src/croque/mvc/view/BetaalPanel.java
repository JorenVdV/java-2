package croque.mvc.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class BetaalPanel extends JPanel {
    private JRadioButton contant;
    private JRadioButton proton;
    private JRadioButton bancontact;

    private JLabel betaalWijze;

    public BetaalPanel(PrijsPanel prijsPanel) {
        betaalWijze = prijsPanel.betaalWijze;

        maakComponenten();
        maakLayout();
        voegListenersToe();
    }

    private void maakComponenten() {
        contant = new JRadioButton("Contant", true);
        proton = new JRadioButton("Proton");
        bancontact = new JRadioButton("Bancontact");
        ButtonGroup betaling = new ButtonGroup();
        betaling.add(contant);
        betaling.add(proton);
        betaling.add(bancontact);
    }

    private void maakLayout() {
        setLayout(new GridLayout(0, 1));
        add(contant);
        add(proton);
        add(bancontact);
    }

    private void voegListenersToe() {
        contant.addItemListener(new BetaalListener());
        proton.addItemListener(new BetaalListener());
        bancontact.addItemListener(new BetaalListener());
    }

    private class BetaalListener implements ItemListener {
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                JRadioButton button = (JRadioButton) e.getSource();
                betaalWijze.setText(button.getActionCommand());
            }
        }
    }
}
