package croque.mvc.view;

import javax.swing.*;
import java.awt.*;

class PrijsPanel extends JPanel {
    private JLabel teBetalen;

    /*
    * Hier is package toegankelijkheid noodzakelijk!
    * Het is geen goed idee om getters te maken voor UI onderdelen!
    */
    JLabel betaalWijze;
    JTextField prijsField;

    public PrijsPanel() {
        maakComponenten();
        maakLayout();
    }

    private void maakComponenten() {
        teBetalen = new JLabel("Te Betalen");
        betaalWijze = new JLabel("Contact");
        betaalWijze.setBackground(Color.CYAN);
        betaalWijze.setOpaque(true);
        prijsField = new JTextField(3);
        prijsField.setHorizontalAlignment(JTextField.RIGHT);
    }

    private void maakLayout() {
        setLayout(new FlowLayout(FlowLayout.RIGHT));
        setBackground(Color.ORANGE);
        add(betaalWijze);
        add(teBetalen);
        add(prijsField);
    }
}

