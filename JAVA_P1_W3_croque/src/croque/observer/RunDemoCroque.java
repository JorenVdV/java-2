package croque.observer;

import croque.observer.model.Croque;
import croque.observer.view.CroqueFrame;

public class RunDemoCroque {
    public static void main(String[] args) {
        Croque croque = new Croque();
        new CroqueFrame(croque);
    }
}
