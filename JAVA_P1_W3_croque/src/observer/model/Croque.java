package observer.model;

import java.util.Observable;

// Deze klasse wordt geobserveerd!

/**
 * Deze klasse bevat alle informatie over een croque die besteld kan worden
 *
 * @author jorenvandevondel
 * @version 0.2
 * 
 */
public class Croque extends Observable {
    private static final int BASIS_PRIJS = 200;
    private static final int KAAS_PRIJS = 50;
    private static final int HAM_PRIJS = 60;
    private static final int ANANAS_PRIJS = 30;

    private boolean metKaas = false;
    private boolean metHam = false;
    private boolean metAnanas = false;

    private String betaalWijze = "Contant";

    public String getBetaalWijze() {
        return betaalWijze;
    }

    public void setBetaalWijze(String wijze) {
        betaalWijze = wijze;
        setChanged();
        notifyObservers(wijze);
    }

    public void setMetKaas(boolean kaas) {
        metKaas = kaas;
        setChanged();
        notifyObservers();
    }

    public void setMetHam(boolean ham) {
        metHam = ham;
        setChanged();
        notifyObservers();
    }

    public void setMetAnanas(boolean ananas) {
        metAnanas = ananas;
        setChanged();
        notifyObservers();
    }
    
    public boolean getMetKaas () {
    	return metKaas;
    }
    
    public boolean getMetHam () {
    	return metHam;
    }
    
    public boolean getMetAnanas () {
    	return metAnanas;
    }

    public String teBetalen() {
        int teBetalen = BASIS_PRIJS;

        if (metKaas) {
            teBetalen += KAAS_PRIJS;
        }
        if (metHam) {
            teBetalen += HAM_PRIJS;
        }
        if (metAnanas) {
            teBetalen += ANANAS_PRIJS;
        }
        return Double.toString((double) teBetalen / 100);
    }

}
