import model.AModel;
import model.BModel;
import view.AView;
import view.BView;
import view.ConsoleView;

public class Spel {
    public static void main(String[] args) {
        AModel a = new AModel();
        BModel b = new BModel();

        new AView(a, b);
        new BView(a, b);
        new ConsoleView(a, b);

      }
}
