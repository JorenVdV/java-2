package model;

import java.util.Observable;

public class AModel extends Observable{
    private int a;

    public int getA() {
        return a;
    }

    public void setA(int a) {
    	if(a == this.a)return;
        this.a = a;
        setChanged();
        notifyObservers();
    }
}
