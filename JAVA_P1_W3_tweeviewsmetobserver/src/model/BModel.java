package model;

import java.util.Observable;

public class BModel extends Observable{
    private int b;

    public int getB() {
        return b;
    }

    public void setB(int b) {
    	if(b == this.b)return;
        this.b = b;
        setChanged();
        notifyObservers();
    }
}
