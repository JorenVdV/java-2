package view;

import model.AModel;
import model.BModel;

import javax.swing.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;
import java.awt.*;

public class AView extends JFrame implements Observer{
    private JTextField aField = new JTextField(4);
    private JTextField productField = new JTextField(10);
    private JButton knop = new JButton("Bereken");

    private AModel a;
    private BModel b;

    public AView(AModel a, BModel b) {
        super("A-view");
        this.a = a;
        this.b = b;
        
        a.addObserver(this);
        b.addObserver(this);

        Label aLabel = new Label("geef A:");
        Label productLabel = new Label("Product van A en B:");
        knop.addActionListener(new AListener());
        productField.setEditable(false);

        JPanel hulpPanel = new JPanel(new GridLayout(0, 2));
        hulpPanel.add(productLabel);
        hulpPanel.add(productField);
        hulpPanel.add(aLabel);
        hulpPanel.add(aField);

        JPanel knopPanel = new JPanel();
        knopPanel.add(knop);

        this.getContentPane().add(hulpPanel, BorderLayout.NORTH);
        this.getContentPane().add(knopPanel, BorderLayout.SOUTH);

        toonFrame();
    }

    private class AListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int waarde;
            try {
                waarde = Integer.parseInt(aField.getText());
            } catch (NumberFormatException ex) {
                waarde = 0;
            }
            a.setA(waarde);
        }
    }

    private void toonFrame() {
        setBounds(200, 200, 160, 80);
        pack();
        setVisible(true);

        // Old school
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

	@Override
	public void update(Observable o, Object arg) {
		productField.setText(Integer.toString(a.getA()*b.getB()));
		
	}

}
