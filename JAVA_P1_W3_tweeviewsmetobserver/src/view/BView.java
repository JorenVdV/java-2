package view;

import model.BModel;
import model.AModel;

import javax.swing.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;
import java.awt.*;

public class BView extends JFrame implements Observer {
    private JTextField bField = new JTextField(4);
    private JTextField somField = new JTextField(10);
    private JButton knop = new JButton("Bereken");

    private AModel a;
    private BModel b;

    public BView(AModel a, BModel b) {
        super("B-view");
        this.a = a;
        this.b = b;
        a.addObserver(this);
        b.addObserver(this);

        Label bLabel = new Label("geef B:");
        Label somLabel = new Label("Som van A en B:");
        knop.addActionListener(new BListener());
        somField.setEditable(false);

        JPanel hulpPanel = new JPanel(new GridLayout(0, 2));
        hulpPanel.add(somLabel);
        hulpPanel.add(somField);
        hulpPanel.add(bLabel);
        hulpPanel.add(bField);

        JPanel knopPanel = new JPanel();
        knopPanel.add(knop);

        this.getContentPane().add(hulpPanel, BorderLayout.NORTH);
        this.getContentPane().add(knopPanel, BorderLayout.SOUTH);

        toonFrame();
    }

    private class BListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int waarde;
            try {
                waarde = Integer.parseInt(bField.getText());
            } catch (NumberFormatException ex) {
                waarde = 0;
            }
            b.setB(waarde);
        }
    }

    private void toonFrame() {
        setBounds(500, 200, 160, 80);
        pack();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
     }

	@Override
	public void update(Observable o, Object arg) {
		somField.setText(Integer.toString(a.getA()+b.getB()));
		
	}
}
