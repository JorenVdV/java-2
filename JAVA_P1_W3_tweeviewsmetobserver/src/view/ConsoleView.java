package view;

import java.util.Observable;
import java.util.Observer;

import model.AModel;
import model.BModel;

public class ConsoleView implements Observer{
	
    private AModel a;
    private BModel b;
    
    
    public ConsoleView(AModel a, BModel b){
    	this.a = a;
    	this.b = b;
    	
    	a.addObserver(this);
    	b.addObserver(this);
    }
 
 
 
	@Override
	public void update(Observable o, Object arg) {
		if(o.equals(a)){
			// a has been changed
			System.out.println(a.getA());
		}else if (o.equals(b)){
			// b has been changed
			System.out.println(b.getB());
		}else{
			System.err.println("No change has been found?");
		}
	}
    
   
}
