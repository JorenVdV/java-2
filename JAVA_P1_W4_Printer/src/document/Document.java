package document;

import java.util.Date;

import printer.*;

/**
 * Abstracte klasse Document die de Printable-interface gebruikt.
 * Afgeleide klassen zijn: TextDocument, SpreadSheet en Drawing.
 */
public abstract class Document implements Printable {
    private int pages;
    private String author;
    private String name;
    private Date created;
    private int size;

    /**
     * Constructor die een nieuw Document aanmaakt.
     */
    public Document(String name, String author, Date created, int pages, int size) {
        this.name = name;
        this.author = author;
        this.created = created;
        this.pages = pages;
        this.size = size;
    }

    /**
     * De methode countPages retourneert het aantal pagina's waaruit het document bestaat
     *
     * @return aantal pagina's
     */
    public int countPages() {
        return pages;
    }

    /**
     * De methode getSize retourneert de grootte (in bytes) van het document
     *
     * @return de grootte van het document
     */
    public int getSize() {
        return size;
    }

    /**
     * De methode print roept de print-methode van de geselecteerde printer op.
     *
     * @throws PrintIOException indien geen printer werd geselecteerd.
     */
    public void print() throws PrintIOException {
        if (Printer.getSelectedPrinter() != null) {
            Printer.getSelectedPrinter().print(this);
        } else {
            throw new PrintIOException("No printer selected!");
        }
    }



    @Override
    public String toString() {
        return name + "\n" +
                "author: " + author + "\n" +
                "created: " + created.toString() + "\n" +
                "pages: " + pages + "\n" +
                "size: " + size;
    }
}