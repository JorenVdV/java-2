package document;

import java.util.Date;

public class Drawing extends Document {

	public Drawing(String name, String author, Date created, int pages, int size) {
		super(name, author, created, pages, size);
	}
	
	public String toString(){
		return String.format("Drawing %s", super.toString());
	}

}
