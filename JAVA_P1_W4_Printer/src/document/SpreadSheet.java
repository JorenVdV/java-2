package document;

import java.util.Date;

public class SpreadSheet extends Document {

	public SpreadSheet(String name, String author, Date created, int pages, int size) {
		super(name, author, created, pages, size);
	}
	
	public String toString(){
		return String.format("SpreadSheet %s", super.toString());
	}

}
