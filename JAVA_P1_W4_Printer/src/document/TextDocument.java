package document;

import java.util.Date;

public class TextDocument extends Document {

	public TextDocument(String name, String author, Date created, int pages, int size) {
		super(name, author, created, pages, size);
	}

	
	public String toString(){
		return String.format("TextDocument %s", super.toString());
	}
}
