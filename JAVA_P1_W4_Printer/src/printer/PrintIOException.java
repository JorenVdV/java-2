package printer;

import java.io.IOException;

/**
 * Een PrintIOException wordt geworpen als er problemen optreden met de printer
 * "out of paper" of als de PrintQueue overbelast of leeg is of als er geen printer geselecteerd werd.
 */
public class PrintIOException extends IOException {

  public PrintIOException() {
  }

  public PrintIOException(String message) {
    super(message);
  }

}