package printer;

import java.util.LinkedList;

/**
 * Pas het Singleton pattern toe op de klasse PrintQueue:
 * want er kan slechts één PrintQueue aanwezig zijn!
 * Ingekapseld: een LinkedList van Printable objecten die werkt volgens het
 * FIFO-algoritme.
 * Methoden enqueue en dequeue om objecten in- en uit de wachtrij te halen.
 */
public class PrintQueue {
     private LinkedList<Printable> queue;

    /**
     * toevoegen van Printable objecten aan de queue, die georganiseerd is
     * volgens het FIFO-algoritme.
     *
     * @throws PrintIOException indien de queue zijn maximum size bereikt heeft.
     */
    public void enqueue(Printable printable) throws PrintIOException {
        //vul aan...
    }

    /**
     * verwijderen van Printable objecten uit de queue, die georganiseerd is
     * volgens het FIFO-algoritme.
     *
     * @throws PrintIOException indien er geen documenten meer in de queue zijn.
     */
    public Printable dequeue() throws PrintIOException {
        //vul aan...
        return null;
    }

    /**
     * afdrukken van alle Printable objecten in de queue, volgens het FIFO-algoritme.
     *
     * @throws PrintIOException indien de printer een probleem heeft.
     */
    public void printAll() throws PrintIOException {
        //vul aan...
    }

    /**
     * Methode emptyQueue verwijdert alle documenten uit de queue.
     */
    public void emptyQueue() {
        //vul aan...
    }

    public String toString() {
        //vul aan...
        return "";
    }
}

