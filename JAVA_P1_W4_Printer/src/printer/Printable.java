package printer;

import document.Document;

/**
 * Gemeenschappelijke interface voor objecten die door de klasse Printer
 * als input worden aanvaard.
 */
public interface Printable {

    void print() throws PrintIOException;

    int countPages();

    int getSize();

}