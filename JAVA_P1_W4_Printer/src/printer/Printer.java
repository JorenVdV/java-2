package printer;

/**
 * De klasse Printer is verantwoordelijk voor het afdrukken van documenten
 * die de Printable-interface implementeren.
 * E�n printer is geselecteerd als default-printer en zal benaderd worden door
 * de PrintQueue: declareer die als klassevariabele
 */
public class Printer {
    private String name;
    private int serial;
    private int pages;
    //declareer de klassevariabele selectedPrinter
    private static Printer selectedPrinter;
    

    /**
     * constructor: maakt een nieuw Printerobject met als attributen;
     * @param name   de naam van de printer
     * @param serial het unieke serienummer van de printer
     */
    public Printer(String name, int serial) {
        this.name = name;
        this.serial = serial;
        this.pages = 0;
    }

    /**
     * De methode select wordt gebruikt om deze printer als default-printer te selecteren.
     */
    public void select() {
        selectedPrinter = this;
    }

    /**
     * De methode getSelectedPrinter retourneert de geselecteerde printer
     */
    public static Printer getSelectedPrinter() {
        return selectedPrinter;
    }

    /**
     * De methode loadPaper om papier in de printer toe te voegen
     * (anders krijg je een "out of paper"-fout bij het afdrukken)
     * * @param pages de hoeveelheid papier die wordt toegevoegd.
     */
    public void loadPaper(int pages) {
    	this.pages+= pages;
    }

    /**
     * De methode paperLeft geeft aan hoeveel blanco pagina's de printer nog ter
     * beschikking heeft.
     *
     * @return het aantal resterende pagina's in de lade.
     */
    public int paperLeft() {
        return this.pages;
    }

    /**
     * De methode print drukt het document in kwestie af (hier gewoon System.out.println...)
     *
     * @throws PrintIOException indien er papier tekort is.
     * @rparam het af te drukken document
     */
    public void print(Printable printable) throws PrintIOException {
        if(printable.countPages() <= pages) pages -= printable.countPages();
        else throw new PrintIOException("Out of paper");
        
        System.out.println("\nDocument printed: " + printable.toString());
        System.out.println("on printer: " + this.toString());
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", this.name, this.serial);
    }
}