package test.printer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * deze klasse test de PrintQueue-klasse uit.
 * Kijk of alle testen foutloos runnen.
 */
/*public class TestPrintQueue {
    private Printer deskjet;
    private Date vandaag;
    private Printable textdoc;
    private Printable drawing;
    private Printable sheet;
    private PrintQueue thisQueue = PrintQueue.getInstance();

    @Before
    public void setUp() throws ParseException {
        deskjet = new Printer("HP Deskjet 930", 45784512);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
        vandaag = formatter.parse("06/05/2004");
        textdoc = new TextDocument("Verslag.doc", "Karel de Grote", vandaag, 5, 128000);
        drawing = new Drawing("Moulin Rouge", "Vincent Van Gogh", vandaag, 1, 1450000);
        sheet = new SpreadSheet("Grafieken.doc", "anoniem", vandaag, 10, 515000);
        thisQueue.setMaxSize(100000000);
    }

    @After
    public void tearDown() {
        thisQueue.emptyQueue();
    }

    @Test
    public void testSingleton() {
        assertNotNull("getInstance geeft null-waarde", thisQueue);
        assertEquals("Fout in singletonpattern", thisQueue, PrintQueue.getInstance());
    }

    @Test(expected = PrintIOException.class)
    public void testLegeQueue() throws PrintIOException {
        thisQueue.dequeue();
        fail("Een job uit een lege queue halen moet een PrintIOException geven");
    }

    @Test(expected = PrintIOException.class)
    public void testVolleQueue() throws PrintIOException {
        thisQueue.setMaxSize(0);
        thisQueue.enqueue(textdoc);
        fail("Een job in een volle queue plaatsten moet een PrintIOException geven");
    }

    private void vulQueue() throws PrintIOException {
        thisQueue.enqueue(textdoc);
        thisQueue.enqueue(drawing);
        thisQueue.enqueue(sheet);
        thisQueue.enqueue(textdoc);
        thisQueue.enqueue(drawing);
        thisQueue.enqueue(sheet);
    }

    @Test
    public void testEnqueue() throws PrintIOException {
        thisQueue.setMaxSize(100000000);
        vulQueue();
        assertTrue("Foutief aantal documenten in queue", thisQueue.count() == 6);
        assertTrue("Foutieve totalsize van queue", thisQueue.totalSize() == 4186000);
    }

    @Test
    public void testDequeue() throws PrintIOException {
        vulQueue();
        thisQueue.dequeue();
        thisQueue.dequeue();
        assertTrue("Aantal jobs in queue moet 4 zijn", thisQueue.count() == 4);
        thisQueue.emptyQueue();
        assertTrue("Queue moet leeg zijn", thisQueue.count() == 0);
    }

    @Test
    public void testToString() {
        assertEquals("Verkeerde output voor toString",
                "0 documents in printqueue", thisQueue.toString());
    }

    @Test
    public void testPrintAll() throws Exception {
        deskjet.select();
        deskjet.loadPaper(40);
        for (int i = 0; i < 3; i++) {
            thisQueue.enqueue(drawing);
        }
        thisQueue.printAll();
        assertTrue("Queue niet leeg na printAll", thisQueue.count() == 0);
    }


}
*/
