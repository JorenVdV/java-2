package test.printer;

import static org.junit.Assert.*;

import java.text.*;
import java.util.*;

import org.junit.*;

import document.TextDocument;
import printer.PrintIOException;
import printer.Printable;
import printer.Printer;

/**
 * deze klasse test de Printer-klasse uit.
 * Kijk of alle testen foutloos runnen.
 */
public class TestPrinter {
    private Printer deskjet;
    private Printer laserjet;
    private Printer matrix;
    private Date vandaag;

    @Before
    public void setUp() throws ParseException {
        deskjet = new Printer("HP Deskjet 930", 45784512);
        laserjet = new Printer("Agfa laserjet 274C", 12312311);
        matrix = new Printer("Brother M150", 7001702);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
        vandaag = formatter.parse("06/05/2004");
    }

    @Test
    public void testSelect() {
        deskjet.select();
        assertTrue("Fout in select", Printer.getSelectedPrinter() == deskjet);
        assertEquals("Fout in getSelectedPrinter", deskjet, Printer.getSelectedPrinter());
        laserjet.select();
        assertEquals("Fout in tweede select", laserjet, Printer.getSelectedPrinter());
    }

    @Test
    public void testLoadPaper() {
        assertTrue("Foutieve papierstatus", deskjet.paperLeft() == 0);
        deskjet.loadPaper(10);
        assertTrue("Foutieve papierstatus na bijvullen", deskjet.paperLeft() == 10);
        deskjet.loadPaper(10);
        assertTrue("Foutieve papierstatus na bijvullen", deskjet.paperLeft() == 20);
    }

    @Test
    public void testToString() {
        assertEquals("Foutieve String", "Brother M150 (7001702)", matrix.toString());
        assertEquals("Foutieve String", "Agfa laserjet 274C (12312311)", laserjet.toString());
    }

    @Test(expected = PrintIOException.class)
    public void testPrint1() throws PrintIOException {
        Printable newDoc = new TextDocument("MyText.doc", "Karel deGrote", vandaag, 5, 128000);
        laserjet.select();
        laserjet.print(newDoc);
        fail("Geen exception gegenereerd n.a.v. papiervoorraad");
    }

    @Test
    public void testPrint() {
        try {
            Printable newDoc = new TextDocument("MyText.doc", "Karel deGrote", vandaag, 5, 128000);
            laserjet.loadPaper(20);
            laserjet.print(newDoc);
            assertTrue("Foutieve papierstatus na afdrukken", laserjet.paperLeft() == 15);
        }
        catch (PrintIOException e) {
            fail("PrintIOException mag niet voorkomen");
        }
    }

    
}
