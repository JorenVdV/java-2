package quarto;

import java.util.HashSet;
import java.util.Set;

import quarto.QuartoBlock.Color;
import quarto.QuartoBlock.Length;
import quarto.QuartoBlock.Shape;
import quarto.QuartoBlock.Volume;

/**
 * Werk uit volgens de instructies.
 */

public class QuartoFactory {
	
	private static HashSet<QuartoBlock> blocks = new HashSet<>();
	private static int count =0; 

	public static QuartoBlock createBlock(Length length, Color color, Shape shape, Volume volume) {
		int hasc = length != null ? length.hashCode() : 0;
		hasc = 31 * hasc + (color != null ? color.hashCode() : 0);
		hasc = 31 * hasc + (shape != null ? shape.hashCode() : 0);
		hasc = 31 * hasc + (volume != null ? volume.hashCode() : 0);
        
        QuartoBlock toreturn =null;
        
        for(QuartoBlock q : blocks){
        	if(q.hashCode() == hasc){
        		toreturn = q;
        		break;
        	}
        }
        
        if(toreturn==null){
        	toreturn = new QuartoBlock(length, color, shape, volume);
        	blocks.add(toreturn);
        	count++;
        }
        
		return toreturn;
	}

	public static String getProductionOverview() {
		String str = String.format("%d blocks generated:", count);
		int c = 1;
		for(QuartoBlock q: blocks){
			str = str+"\n"+Integer.toString(c++)+") " +q.toString();
		}
		return str;
	}


}

