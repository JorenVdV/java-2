package be.kdg.demo.singleton;


/**
 * Vul deze klasse aan.
 */
public class UniekeVerzameling extends Verzameling{
	private static UniekeVerzameling uv = null;
	
    private UniekeVerzameling(){
    	super();
    }
    
    public synchronized static UniekeVerzameling  getInstance(){
    	if(uv == null){
    		uv = new UniekeVerzameling();
    	}
    	return uv;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException{
    	throw new CloneNotSupportedException();
    }
    
    
    
}
