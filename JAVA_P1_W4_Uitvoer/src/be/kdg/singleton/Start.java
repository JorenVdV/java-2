package be.kdg.singleton;

import be.kdg.singleton.view.DocumentFrame;

public class Start {
    public static void main(String[] args) {
        new DocumentFrame();
    }
}
