package be.kdg.singleton.model;

/**
 * Hier moet niets gewijzigd worden!
 */
public class Document {
    private String titel;
    private String inhoud;

    public Document(String titel, String inhoud) {
        this.titel = titel;
        this.inhoud = inhoud;
    }

    public String getTitel() {
        return titel;
    }

    public String getInhoud() {
        return inhoud;
    }

    @Override
    public String toString() {
        return titel;
    }
}
