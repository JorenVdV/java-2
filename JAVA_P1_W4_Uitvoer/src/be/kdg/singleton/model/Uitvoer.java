package be.kdg.singleton.model;

import javax.swing.*;

/*
 * Maak van deze klasse een singleton zodat je de functionaliteit overal kan oproepen.
 */
public class Uitvoer {
	private static Uitvoer uit = null;
	
	private Uitvoer(){}
	
	public static synchronized Uitvoer getInstance(){
		if(uit == null) uit =  new Uitvoer();
		return uit;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{
		throw new CloneNotSupportedException();
	}
	
	
	
    public void naarPrinter(Document document) {
        JOptionPane.showMessageDialog(null, "'" + document + "' wordt geprint");
    }

    public void naarStandardOutput(Document document) {
        System.out.println(document);
    }

    public void naarStandardError(Document document) {
        System.err.println(document);
    }
}
