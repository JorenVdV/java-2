package be.kdg.singleton.view;

import be.kdg.singleton.model.Document;
import be.kdg.singleton.model.Uitvoer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DocumentFrame extends JFrame {
    private JTextField titelField;
    private JTextArea tekstField;
    private JButton printerButton;
    private JButton standardOutButton;
    private JButton standardErrButton;

    public DocumentFrame() {
        super("documentenframe");
        maakComponenten();
        layoutCompontenten();
        addListeners();
        setSize(400, 200);
        setLocationRelativeTo(this);
        setVisible(true);
    }

    private void maakComponenten() {
        this.titelField = new JTextField();
        this.tekstField = new JTextArea();
        this.printerButton = new JButton("Printer");
        this.standardOutButton = new JButton("Standard Output");
        this.standardErrButton = new JButton("Standard Error");
    }

    private void layoutCompontenten() {
        JLabel titelLabel = new JLabel("titel:");
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(titelLabel, BorderLayout.WEST);
        panel.add(titelField, BorderLayout.CENTER);
        this.add(panel, BorderLayout.NORTH);
        JLabel tekstLabel = new JLabel("tekst:");
        panel = new JPanel(new BorderLayout());
        panel.add(tekstLabel, BorderLayout.NORTH);
        panel.add(new JScrollPane(tekstField), BorderLayout.CENTER);
        this.add(panel, BorderLayout.CENTER);
        panel = new JPanel(new GridLayout(1, 3));
        panel.add(printerButton);
        panel.add(standardOutButton);
        panel.add(standardErrButton);
        this.add(panel, BorderLayout.SOUTH);
    }

    private void addListeners() {
        printerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                naarPrinter();
            }
        });
        standardOutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                naarStandardOut();
            }
        });
        standardErrButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                naarStandardErr();
            }
        });
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void naarStandardErr() {
        Document document = new Document(titelField.getText(), tekstField.getText());
        Uitvoer uit = Uitvoer.getInstance();
        uit.naarStandardError(document);
    }

    private void naarStandardOut() {
        Document document = new Document(titelField.getText(), tekstField.getText());
        Uitvoer uit = Uitvoer.getInstance();
        uit.naarStandardOutput(document);
    }

    private void naarPrinter() {
        Document document = new Document(titelField.getText(), tekstField.getText());
        Uitvoer uit = Uitvoer.getInstance();
        uit.naarPrinter(document);
    }
}
