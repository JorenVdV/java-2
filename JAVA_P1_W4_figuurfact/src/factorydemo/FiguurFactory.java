package factorydemo;

import factorydemo.Rechthoek;

public class FiguurFactory {
	
	public FiguurFactory(){
		
	}
	
	public static Figuur getFiguur(FiguurType type, double zijde){
		
		return null;
	}
	
	public static Figuur getFiguur(FiguurType type, double dimEen, double dimTwee){
		switch (type) {
		case RECHTHOEK:
			return Rechthoek.newRechthoek(dimEen, dimTwee);
		
		case RUIT:
			return Ruit.newRuit(dimEen, dimTwee);
		
		case VIERKANT:
			return Vierkant.newVierkant(dimEen);
			
		default:
			return null;
		}
	}
	
	public static Figuur getRechthoek(double zijde){
		return Rechthoek.newRechthoek(zijde);
	}
	
	public static Figuur getRechthoek(double breedte, double hoogte){
		return Rechthoek.newRechthoek(breedte, hoogte);
	}
	
	public static Figuur getVierkant(double zijde){
		return Vierkant.newVierkant(zijde);
	}
	
}
