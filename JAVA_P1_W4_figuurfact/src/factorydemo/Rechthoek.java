package factorydemo;

public class Rechthoek implements Figuur{
	private double b;
	private double h;
	
	private Rechthoek(double b, double h){
		this.b = b;
		this.h = h;
	}
	
	public static Rechthoek newRechthoek(double z){
		return new Rechthoek(z, z);
	}
	
	public static Rechthoek newRechthoek(double b, double h){
		return new Rechthoek(b, h);
	}
	
	public double getBreedte(){
		return b;
	}
	
	public double getHoogte(){
		return h;
	}
	
	public double oppervlakte(){
		return b*h;
	}
	
	public String toString(){
		return String.format("rechthoek %f %f",b, h);
	}
	
	
}
