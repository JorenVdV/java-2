package factorydemo;

public class Ruit implements Figuur{
	private double g;
	private double k;
	
	private Ruit(double g, double k){
		this.g = g;
		this.k = k;
	}
	
	public static Ruit newRuit(double groot, double klein){
		return new Ruit(groot, klein);
	}
	
	public double getGroteDiagonaal(){
		return g;
	}
	
	public double getKleineDiagonaal(){
		return k;
	}
	
	public double oppervlakte(){
		return g*k*0.5;
	}
	
	public String toString(){
		return String.format("ruit %f %f", g, k);
	}
}
