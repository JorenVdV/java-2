package factorydemo;

public class Vierkant implements Figuur{
	private double z;
	
	private Vierkant(double z){
		this.z = z;
	}

	public static Vierkant newVierkant(double zijde){
		return new Vierkant(zijde);
	}
	
	public double getZijde(){
		return z;
	}
	
	public double oppervlakte(){
		return z*z;
	}
	
	public String toString(){
		return String.format("vierkant %s", z);
	}
}
