package be.kdg.laptop;

import java.util.Comparator;

public final class Laptop implements Comparable<Laptop> {
	private String naam;
	private String processor;
	private int ram;
	private int hardDisk;
	private double inch;
	private double prijs;

	public Laptop(String naam, String processor, int ram, int hardDisk, double inch, double prijs) {
		this.naam = naam;
		this.processor = processor;
		this.ram = ram;
		this.hardDisk = hardDisk;
		this.inch = inch;
		this.prijs = prijs;
	}

	public String getNaam() {
		return naam;
	}

	public double getPrijs() {
		return prijs;
	}

	public String getProcessor() {
		return processor;
	}

	public int getRam() {
		return ram;
	}

	public int getHardDisk() {
		return hardDisk;
	}

	public double getInch() {
		return inch;
	}

	@Override
	public String toString() {
		return String.format("%-30s (%-30s RAM:%2dGB HD:%4dGB %5.1finch %8.2f)", naam, processor, ram, hardDisk, inch,
				prijs);
	}

	public static class Comparators {

		public static Comparator<Laptop> PRIJS = new Comparator<Laptop>() {
			@Override
			public int compare(Laptop o1, Laptop o2) {
				return o1.prijs >= o2.prijs ? 1 : 0;
			}
		};

	}

	@Override
	public int compareTo(Laptop o) {
		return 0;
	}

}
