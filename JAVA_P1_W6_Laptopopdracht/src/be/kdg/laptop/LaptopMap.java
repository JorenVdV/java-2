package be.kdg.laptop;

import java.util.*;

public final class LaptopMap {
	public enum SortCriterium {
		OP_NAAM, OP_PRIJS
	}

	private Map<String, Laptop> map;

	public LaptopMap() {
		map = new TreeMap<>();
	}

	public int getAantal() {
		return map.size();
	}

	public void voegToe(Laptop nieuw) {
		if (map.containsValue(nieuw)) {
			throw new IllegalArgumentException(nieuw.getNaam() + " komt reeds voor in map");
		}
		map.put(nieuw.getNaam(), nieuw);
	}

	public void verwijder(String naam) {
		Laptop laptop = getLaptop(naam);
		if (laptop != null) {
			map.remove(laptop.getNaam());
		}
	}

	public Laptop getLaptop(String naam) {
		return map.get(naam);
	}

	public List<Laptop> getList(SortCriterium criterium) {
		// Opdracht 3
		ArrayList<Laptop> toreturn = new ArrayList<>();
		for(Map.Entry<String, Laptop> entry: map.entrySet()){
			toreturn.add(entry.getValue());
		}
		
		switch (criterium) {
		case OP_NAAM:
			return toreturn;
		case OP_PRIJS:{
			Collections.sort(toreturn, Laptop.Comparators.PRIJS);
			return toreturn;
		}

		default:
			return Collections.emptyList();
		}

		
	}

}
