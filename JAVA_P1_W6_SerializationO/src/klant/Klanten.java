package klant;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Werk in deze klasse de methode serialize en deserialize uit.
 * Vergeet ook niet dat bepaalde klassen de Serializable interface moeten implementeren!
 */
public class Klanten{
    /**
	 * 
	 */
	private static final String FILENAME = "data\\klantendata.txt";
    private Map<Integer, Klant> klantenMap = new HashMap<>();

    public void voegToe(Klant nieuw) {
        if (klantenMap.containsKey(nieuw.getKlantNr())) {
            System.out.println("KlantenNr is niet uniek");
            return;
        }
        klantenMap.put(nieuw.getKlantNr(), nieuw);
    }

    public Klant verwijderKlant(Klant oud) {
        return klantenMap.remove(oud.getKlantNr());
    }

    public void verwijderAlles() {
        klantenMap = new HashMap<>();
    }

    public Klant zoekKlant(Klant zoek) {
        return klantenMap.get(zoek.getKlantNr());   //null indien niet gevonden
    }

    public void updateKlant(Klant klant) {
        if (!klantenMap.containsKey(klant.getKlantNr())) {
            System.out.println("Klant komt niet voor!");
            return;
        }
        klantenMap.put(klant.getKlantNr(), klant); //overschrijven bestaande klantgegevens
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Klanten:\n");
        for (Klant k : klantenMap.values()) {
            builder.append("\t");
            builder.append(k.toString());
            builder.append("\n");
        }
        return builder.toString();
    }

    public void serialize(){
    	try ( FileOutputStream fileOut = new FileOutputStream(FILENAME);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);){
              out.writeObject(this.klantenMap);
              out.close();
              fileOut.close();
          } catch (IOException ex) {
              ex.printStackTrace();
          }
        //uitwerken, maak gebruik van de constante FILENAME
    }

    @SuppressWarnings("unchecked")
	public void deserialize() {
    	try (FileInputStream fileIn = new FileInputStream(FILENAME);
                ObjectInputStream in = new ObjectInputStream(fileIn);) {
               this.klantenMap = (Map<Integer, Klant>) in.readObject();
               fileIn.close();
               in.close();
           } catch (IOException ex) {
               ex.printStackTrace();
           } catch (ClassNotFoundException ex) {
               System.out.println("Klanten klasse niet gevonden");
               ex.printStackTrace();
           }
        //uitwerken, maak gebruik van de constante FILENAME
    }
}
