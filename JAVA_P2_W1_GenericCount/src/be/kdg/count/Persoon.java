package be.kdg.count;

public class Persoon {
	private String name;
	private int age;
	
	public Persoon(){
		this.name="";
		this.age=0;
	}
	public Persoon(String name, int age){
		this.name=name;
		this.age=age;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass()!=this.getClass())return false;
		Persoon other = (Persoon)o;
		return (this.name.equals(other.name)&&this.age==other.age);
	}
	
	@Override
	public String toString(){
		return String.format("%s %d", name, age);
	}
}
