package stackimpl;

public class LimitedNumericStack<T extends Number> extends LimitedStack<T> {
	
	public LimitedNumericStack(){
		super();
	}
	
	public LimitedNumericStack(int size){
		super(size);
	}
	
	public void push(T nieuw) {
		super.push(nieuw);
	}
	
	 public T pop() {
		 return super.pop();
	 }
	 
	 public T top() {
		 return super.top();
	 }
	
	
}
