package stackimpl;

public class MyNumericStack<T extends Number> {
	private LimitedStack<T> stack;

	public MyNumericStack() {
		stack = new LimitedStack<>();
	}

	public MyNumericStack(int size) {
		stack = new LimitedStack<>(size);
	}

	public void push(T nieuw) {
		stack.push(nieuw);
	}

	public T pop() {
		return stack.pop();
	}

	public T top() {
		return stack.top();
	}

	public int capacity() {
		return stack.capacity();
	}

	public int size() {
		return stack.size();
	}

	@Override
	public String toString() {
		return stack.toString();
	}
}
