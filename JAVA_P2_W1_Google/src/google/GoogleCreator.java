package google;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

/**
 * Deze klasse hoef je niet aan te vullen.  Ze wordt gebruikt om een Google object te maken
 * (met de methode createGoogle).  Je geeft als parameter een path op en de methode zal dan
 * het path recursief doorlopen en alle bestanden indexeren.  De methode gaat in ieder bestand
 * op zoek naar woorden en associeert dan dat woord met het bestand.
 */
public class GoogleCreator {
    private GoogleCreator() {
    }

    public static Google createGoogle(String path) {
        Google google = new Google();
        File file = new File(path);
        addFile(file, google);
        return google;
    }

    private static void addFile(File file, Google google) {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for(File subfile : files) {
                addFile(subfile, google);
            }
        } else if (file.isFile()) {
            parseFile(file, google);
        }
    }

    private static void parseFile(File file, Google google) {
        try {
            StreamTokenizer tokenizer = new StreamTokenizer(new FileReader(file));
            int token = tokenizer.nextToken();
            while (token != StreamTokenizer.TT_EOF) {
                if (token == StreamTokenizer.TT_WORD) {
                    String keyword = tokenizer.sval;
                    google.add(keyword, file);
                }
                token = tokenizer.nextToken();
            }
        } catch (IOException e) {
            // do nothing, file cannot be indexed
        }
    }
}