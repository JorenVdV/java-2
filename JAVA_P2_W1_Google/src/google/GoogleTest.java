 package google;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Set;

public class GoogleTest {
    public static void main(String[] args) throws IOException {
        Google google = GoogleCreator.createGoogle("./files");
        System.out.println("Geef een zoek-string in:");
        Scanner scanner = new Scanner(System.in);
        String searchString = scanner.next();
        Set<File> result = google.search(searchString);
        System.out.println("Het resultaat is:");
        for(File file: result) {
            System.out.println(file.getCanonicalPath());
        }
    }
}
