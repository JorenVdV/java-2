package model;

import java.util.Comparator;

public class Optreden {
    private String naam;
    private String podium;
    private int uur;
    private int min;
    private int sterren;

    public Optreden(String naam, String podium, int uur, int min, int sterren) {
        this.naam = naam;
        this.podium = podium;
        this.uur = uur;
        this.min = min;
        this.sterren = sterren;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(naam);
        sb.append(" (");
        sb.append(podium);
        sb.append(", ");
        sb.append(uur);
        sb.append("u");
        if (min > 0) sb.append(min);
        sb.append(")--> ");
        for (int i = 0; i < sterren; i++) {
            sb.append("*");
        }
        return sb.toString();
    }
    
    public static class Comparators {
        public static final Comparator<Optreden> NAME = (Optreden o1, Optreden o2) -> o1.naam.compareTo(o2.naam);
//        public static final Comparator<Optreden> UUR = (Optreden o1, Optreden o2) -> Integer.compare(o1.age, o2.age);
        public static final Comparator<Optreden> STERREN = (Optreden o1, Optreden o2) -> Integer.compare(o1.sterren, o2.sterren);
    }
    
    public int compareTo(Optreden other) {
    	
		
		//ascending order
		return this.sterren- other.sterren;
		
		//descending order
		//return compareQuantity - this.quantity;
		
	}


}
