package be.kdg.acteur;

import java.util.Objects;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8/11/2015 17:57
 *
 */

public class Acteur implements Comparable<Acteur> {
    private final String naam;
    private final int geboorteJaar;

    public Acteur(String naam, int geboorteJaar) {
        this.naam = naam;
        this.geboorteJaar = geboorteJaar;
    }

    public String getNaam() {
        return naam;
    }

    public int getGeboorteJaar() {
        return geboorteJaar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Acteur acteur = (Acteur) o;
        return geboorteJaar == acteur.geboorteJaar &&
                Objects.equals(naam, acteur.naam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam, geboorteJaar);
    }

    public int compareTo(Acteur andereActeur) {
        int jaarVerschil = Integer.compare(geboorteJaar, andereActeur.geboorteJaar);
        if (jaarVerschil != 0) return jaarVerschil;
        return naam.compareTo(andereActeur.naam);
    }

    @Override
    public String toString() {
        return geboorteJaar + "  " + naam;
    }
}

