import employee.Data;
import employee.Employee;
import employee.Gender;
import employee.Role;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Vervolledig de volgende code. Maak telkens gebruik van een Stream object op basis van myList:
 */
public class DemoStreams {
    public static void main(String[] args) {
        List<Employee> myList = Data.getDataAsList();

        System.out.println("Alle vrouwen van Executive volgens leeftijd:");
        myList.stream()
        	.filter(e->e.getGender().equals(Gender.FEMALE))
        	.filter(e->e.getRole().equals(Role.EXECUTIVE))
        	.sorted(Comparator.comparing(Employee::getAge))
        	.forEach(System.out::println);
        
        double result = myList.stream()
        		.filter(e->e.getDept().equalsIgnoreCase("sales"))
        		.mapToDouble(Employee::getAge)
        		.average()
        		.getAsDouble();
        System.out.printf("\nGemiddelde leeftijd van de Sales werknemers: %.1f\n", result);

        String str = myList.stream()
        		.filter(e->e.getRole().equals(Role.MANAGER))
        		.sorted(Comparator.comparing(Employee::getFullName))
        		.map(Employee::getFullName)
        		.collect(Collectors.joining(", "));

        System.out.println("\nAlfabetische opsomming van de managers:\n" + str);

        long aantal = myList.stream()
        		.filter(e->e.getGender().equals(Gender.MALE))
        		.filter(e->e.getDept().equalsIgnoreCase("ENG"))
        		.count();

        System.out.println("\nHet aantal mannen op de afdeling \"ENG\": " + aantal);

    }
}
