package be.kdg;

import be.kdg.film.Data;
import be.kdg.film.Film;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8/11/2015 17:34
 */
public class TestFilms {
	public static void main(String[] args) {
		Map<String, Film> filmMap = new TreeMap<>();

		// Testgegevens
		List<Film> data = Data.getDataList();

		// Maak map (key = titel + jaar, value = filmobject)
		for (Film film : data) {
			filmMap.put(film.getKey(), film);
		}

		// Toon de inhoud van de map met behulp van een Stream!
		// Maak een stream op basis van de values van de map en
		// gebruik vervolgens de forEach-methode van de Stream<T>
		// interface
		// Signatuur: void forEach(Consumer<? Super T> action)
		filmMap.values().stream().forEach(System.out::println);

	}
}

/*
 * Getaway, The : 1960 : Bluray Getaway, The : 1972 : Bluray Hannibal : 2001 :
 * Bluray met BD Live Italian Job, The : 2003 : Bluray met BD Live Lola Rennt :
 * 1998 : Dvd Psycho : 1960 : Dvd Psycho : 1998 : Bluray met BD Live
 */