package be.kdg.film;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8-nov-2009
 */
public class Bluray extends Film {
    private boolean bdLive;

    public Bluray(String titel, int jaar, boolean extras) {
        super(titel, jaar);
        bdLive = extras;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append(" : Bluray");
        if (bdLive) builder.append(" met BD Live");

        return  builder.toString();
    }
}

