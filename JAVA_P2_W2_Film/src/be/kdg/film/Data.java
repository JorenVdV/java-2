package be.kdg.film;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8-nov-2009
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Data {
    private static final List<Film> lijst;

    static {
        lijst = new ArrayList<>();
        lijst.add(new Bluray("Getaway, The", 1972, false));
        lijst.add(new Dvd("Psycho", 1960));
        lijst.add(new Bluray("Hannibal", 2001, true));
        lijst.add(new Dvd("Lola Rennt", 1998));
        lijst.add(new Bluray("Italian Job, The", 2003, true));
        lijst.add(new Bluray("Psycho", 1998, true));
        lijst.add(new Bluray("Getaway, The", 1960, false));
    }

    public static List<Film> getDataList() {
        return Collections.unmodifiableList(lijst);
    }
}
