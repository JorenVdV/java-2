package be.kdg.film;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8-nov-2009
 */

public class Dvd extends Film {

    public Dvd(String titel, int jaar) {
        super(titel, jaar);
    }

    public String toString() {
        return super.toString() + " : Dvd";
    }
}
