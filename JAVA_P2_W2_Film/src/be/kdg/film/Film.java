package be.kdg.film;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8-nov-2009
 */
public class Film {
    private String titel;
    private int jaar;

    public Film(String titel, int jaar) {
        this.titel = titel;
        this.jaar = jaar;
    }

    public String toString() {
        return titel + " : " + jaar;
    }

    public String getKey() {
        return titel + jaar;
    }
}

