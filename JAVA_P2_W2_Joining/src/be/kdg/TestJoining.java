package be.kdg;

import be.kdg.joining.Piloot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Kristiaan Behiels
 * @version 1.0 6/11/2015 23:23
 */

// Zie tekstbestand voor de opdracht
public class TestJoining {
    public static void main(String[] args) {
        List<Piloot> drivers = Arrays.asList(
                new Piloot("Hamilton", 44), new Piloot("Rosberg", 6), new Piloot("Vettel", 5),
                new Piloot("Räikkönen", 7), new Piloot("Bottas", 77), new Piloot("Massa", 19),
                new Piloot("Ricciarddo", 3), new Piloot("Kvyat", 26));

        String namen = drivers.stream()
        		.filter(e->e.getNaam().length()<8)
        		.sorted(Comparator.comparing(Piloot::getNaam))
        		.map(Piloot::getNaam)
        		.collect(Collectors.joining(", "));// Vul hier aan.


        System.out.println(namen);
    }
}

/*
Bottas, Kvyat, Massa, Rosberg, Vettel
 */