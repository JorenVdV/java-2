package be.kdg.joining;

/**
 * @author Kristiaan Behiels
 * @version 1.0 6/11/2015 19:26
 */
public class Piloot {
    private String naam;
    private int nummer;

    public Piloot(String naam, int nummer) {
        this.naam = naam;
        this.nummer = nummer;
    }

    public String getNaam() {
        return naam;
    }

    public int getNummer() {
        return nummer;
    }

    @Override
    public String toString() {
        return String.format("%-10s %2d", naam, nummer);
    }
}
