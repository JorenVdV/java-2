package be.kdg;

import be.kdg.mandje.Artikel;
import be.kdg.mandje.Mandje;

/**
 * @author Kristiaan Behiels
 * @version 1.0 8/11/2015 17:02
 *
 * Zie ook opgave Map P2W1
 */
public class TestMandje {
    public static void main(String[] args) {
        Mandje mandje = new Mandje();

        mandje.voegToe(new Artikel("Druiven", 1.95));
        mandje.voegToe(new Artikel("Passievruchten", 2.35));
        mandje.voegToe(new Artikel("Mandarijnen", 2.50));
        mandje.voegToe(new Artikel("Pruimen", 2.49));
        mandje.voegToe(new Artikel("Druiven", 2.0));
        mandje.voegToe(new Artikel("Peren", 1.99));
        mandje.voegToe(new Artikel("Appels", 2.20));

        System.out.println("\nGesorteerd volgens naam:");
        mandje.toon();

        System.out.println("\nGesorteerd volgens prijs:");
        mandje.toonGesorteerdVolgensPrijs();
    }
}

/*
Gesorteerd volgens naam:
Appels           2,20
Druiven          1,95
Mandarijnen      2,50
Passievruchten   2,35
Peren            1,99
Pruimen          2,49

Gesorteerd volgens prijs:
Druiven          1,95
Peren            1,99
Appels           2,20
Passievruchten   2,35
Pruimen          2,49
Mandarijnen      2,50
 */