package be.kdg.mandje;

/**
 * De klasse Artikel bevat twee attributen,
 * de naam van het artikel (type String) en de prijs in euro (type double).
 *
 * @author Kristiaan Behiels
 * @version 1.0 19-okt-2006
 */
public class Artikel {
    private String naam;
    private double prijs;


    public Artikel(String naam, double prijs) {
        this.naam = naam;
        this.prijs = prijs;
    }

    public String getNaam() {
        return naam;
    }

    public double getPrijs() {
        return prijs;
    }

	@Override
	public String toString() {
		return String.format("%-10s \t %.2f", naam, prijs);
	}
    
    
}


