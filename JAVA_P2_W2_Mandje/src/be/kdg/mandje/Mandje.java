package be.kdg.mandje;

import java.util.*;

/**
 * De klasse mandje bevat een verzameling van Artikel-objecten. Aan deze
 * verzameling moet je artikels kunnen toevoegen en ook artikels kunnen uit
 * verwijderen. Een artikel met dezelfde naam kan slechts éénnmaal toegevoegd
 * worden.
 * <p/>
 * In dit geval bestaat de implementatie uit een TreeMap, waarbij de key de naam
 * van het artikel is en de value het Artikel object.
 *
 * @author Kristiaan Behiels
 * @version 1.0 21-okt-2006 --> 1.1 08/11/2015
 */
public class Mandje {
	private Map<String, Artikel> mandje;

	public Mandje() {
		mandje = new TreeMap<>();
	}

	/*
	 * Deze methode geeft de inhoud van het mandje (values van de Map) in de
	 * vorm van een List terug.
	 */
	public List<Artikel> getMandje() {
		return new ArrayList<>(mandje.values());
	}

	/*
	 * Deze methode voegt een artikel aan het mandje toe ( maar alleen als er
	 * nog geen artikel met dezelfde naam in voorkomt ).
	 */
	public void voegToe(Artikel artikel) {
		if (!mandje.containsKey(artikel.getNaam()))
			mandje.put(artikel.getNaam(), artikel);
	}

	/*
	 * Deze methode toont de inhoud van het mandje, gesorteerd volgens naam,
	 * netjes in kolommen onder mekaar met de prijzen afgerond in centen. Hier
	 * moet je gebruik maken van een stream op basis van de values!
	 */
	public void toon() {
		mandje.values().stream().forEach(System.out::println);
	}

	/*
	 * Deze methode toont de inhoud van het mandje gesorteerd volgens dalende
	 * prijs, netjes in kolommen onder mekaar met de prijzen afgerond in centen.
	 * Hier moet je gebruik maken van een stream op basis van de values!
	 * 
	 */
	public void toonGesorteerdVolgensPrijs() {
    	 mandje.values().stream()
    		.sorted((f1, f2) -> Double.compare(f2.getPrijs(), f1.getPrijs()))
    		.forEach(System.out::println);
    				
    }
}
