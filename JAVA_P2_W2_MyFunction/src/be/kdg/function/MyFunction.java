package be.kdg.function;

import java.util.List;
import java.util.function.Function;

/**
 * @author Kristiaan Behiels
 * @version 1.0 24/11/2015 18:16
 */
public class MyFunction {
    public int somNummers(List<Piloot> piloten, Function<Piloot, Integer> som) {
        int totaal = 0;
        for (Piloot piloot : piloten) {
            totaal += som.apply(piloot);
        }
        return totaal;
    }
}
