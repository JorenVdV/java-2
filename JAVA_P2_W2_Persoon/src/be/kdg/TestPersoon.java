package be.kdg;

import be.kdg.persoon.Geslacht;
import be.kdg.persoon.Personen;
import be.kdg.persoon.Persoon;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Kristiaan Behiels
 * @version 1.0 13/11/2015 18:27
 */
public class TestPersoon {
    public static void printPersonen(
            final String string, final List<Persoon> lijst) {

        System.out.println(string);
        lijst.forEach(System.out::println);
        System.out.println();
    }

    public static void main(String[] args) {

        // Maak een lijst van Persoon objecten
        final List<Persoon> personen = Personen.getPersonen();

        // Sorteer volgens oplopende leeftijd (vul aan)
        Comparator<Persoon> vergelijkOplopend = Persoon::leeftijdsverschil;
        List<Persoon> oplopendeLeeftijd =
                personen.stream()
                .sorted(vergelijkOplopend)
                .collect(Collectors.toCollection(ArrayList::new));


        printPersonen("Gesorteerd volgens oplopende leeftijd: ", oplopendeLeeftijd);

        // Sorteer volgens dalende leeftijd (vul aan)

        List<Persoon> dalendeleeftijd =
                personen.stream()
                .sorted((e1,e2)->e2.getLeeftijd()-e1.getLeeftijd())
                .collect(Collectors.toCollection(ArrayList::new));


        printPersonen("Gesorteerd volgens dalende leeftijd: ", dalendeleeftijd);
        // Geef de jongste persoon (vul aan)
        personen.stream()
        .min(Comparator.comparing(Persoon::getLeeftijd))
        .ifPresent(System.out::println);


        // Geef de oudste persoon (vul aan)
        personen.stream()
        .max(Comparator.comparing(Persoon::getLeeftijd))
        .ifPresent(System.out::println);
        System.out.println();

        // Gesorteerd volgens leeftijd en vervolgens op naam (vul aan)
        printPersonen("Gesorteerd volgens leeftijd en naam",
                personen.stream()
                .sorted(Comparator.comparing(Persoon::getNaam))
                .sorted(Comparator.comparing(Persoon::getLeeftijd))
                .collect(Collectors.toCollection(ArrayList::new))

        );

        // Gesorteerd volgens geslacht en vervolgens op leeftijd (vul aan)
        final Function<Persoon, Geslacht> volgensGeslacht = Persoon::getGeslacht;
        final Function<Persoon, Integer> volgensLeeftijd = Persoon::getLeeftijd;
        final Function<Persoon, String> volgensNaam = Persoon::getNaam;

        printPersonen("Gesorteerd volgens geslacht, leeftijd en naam",
                personen.stream()
                        .sorted(Comparator.comparing(volgensNaam))
                        .sorted(Comparator.comparing(volgensLeeftijd))
                        .sorted(Comparator.comparing(volgensGeslacht))
                        .collect(Collectors.toCollection(ArrayList::new))

        );
    }
}

/*
Gesorteerd volgens oplopende leeftijd:
Jef       - M - 19
Benjamin  - M - 21
Eline     - V - 24
Alicia    - V - 24
Benjamin  - M - 24
Lisa      - V - 25
Stephanie - V - 25
Jos       - M - 55
Roza      - V - 68

Gesorteerd volgens dalende leeftijd:
Roza      - V - 68
Jos       - M - 55
Lisa      - V - 25
Stephanie - V - 25
Eline     - V - 24
Alicia    - V - 24
Benjamin  - M - 24
Benjamin  - M - 21
Jef       - M - 19

Jongste: Jef       - M - 19
Oudste: Roza      - V - 68

Gesorteerd volgens leeftijd en naam
Jef       - M - 19
Benjamin  - M - 21
Alicia    - V - 24
Benjamin  - M - 24
Eline     - V - 24
Lisa      - V - 25
Stephanie - V - 25
Jos       - M - 55
Roza      - V - 68

Gesorteerd volgens geslacht, leeftijd en naam
Jef       - M - 19
Benjamin  - M - 21
Benjamin  - M - 24
Jos       - M - 55
Alicia    - V - 24
Eline     - V - 24
Lisa      - V - 25
Stephanie - V - 25
Roza      - V - 68
*/