package be.kdg;

import be.kdg.persoon.Personen;
import be.kdg.persoon.Persoon;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

/**
 * @author Kristiaan Behiels
 * @version 1.0 13/11/2015 19:22
 */
public class TestPersoon2 {
    public static void main(String[] args) {

        // Maak een lijst van Persoon objecten (reeds gegeven)
        final List<Persoon> personen = Personen.getPersonen();

        // Maak een map met naam en leeftijd als key en persoon als value en druk ze af (vul aan)
//        Map<String, Persoon> map =
//                personen.stream()

        // Groepeer de personen per leeftijd (vul aan)
//        Map<Integer, List<String>> personenPerLeeftijd =
//                personen.stream()


//        personenPerLeeftijd.forEach((k, v) -> System.out.printf("%2d: %s\n", k,
//                v.stream()


        System.out.println();

//        // Tel het aantal personen per leeftijd (vul aan)
//        TreeMap<Integer, Long> perLeeftijd =
//                personen.stream()


//        perLeeftijd.forEach((k, v) -> System.out.printf("%2d -> %d\n", k, v));
    }
}

/*
Lisa-25      -> Lisa      - V - 25
Alicia-24    -> Alicia    - V - 24
Benjamin-24  -> Benjamin  - M - 24
Roza-68      -> Roza      - V - 68
Benjamin-21  -> Benjamin  - M - 21
Jos-55       -> Jos       - M - 55
Eline-24     -> Eline     - V - 24
Stephanie-25 -> Stephanie - V - 25
Jef-19       -> Jef       - M - 19

19: Jef
68: Roza
21: Benjamin
55: Jos
24: Alicia, Benjamin, Eline
25: Lisa, Stephanie

19 -> 1
21 -> 1
24 -> 3
25 -> 2
55 -> 1
68 -> 1

 */