package be.kdg.persoon;

/**
 * @author Kristiaan Behiels
 * @version 1.0 13/11/2015 22:36
 */
public enum Geslacht {
    M, V
}
