package be.kdg.persoon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kristiaan Behiels
 * @version 1.0 13/11/2015 22:39
 */
public class Personen {
    private static final List<Persoon> personen = Arrays.asList(
            new Persoon("Jos", Geslacht.M, 55),
            new Persoon("Eline", Geslacht.V,  24),
            new Persoon("Alicia", Geslacht.V, 24),
            new Persoon("Jef", Geslacht.M, 19),
            new Persoon("Benjamin", Geslacht.M, 21),
            new Persoon("Benjamin", Geslacht.M, 24),
            new Persoon("Roza", Geslacht.V, 68),
            new Persoon("Lisa", Geslacht.V, 25),
            new Persoon("Stephanie", Geslacht.V, 25)
    );

    public static List<Persoon> getPersonen() {
        return new ArrayList<>(personen);
    }
}
