package be.kdg.persoon;

/**
 * @author Kristiaan Behiels
 * @version 1.0 13/11/2015 18:28
 */
public class Persoon {
    private final String naam;
    private final Geslacht geslacht;
    private final int leeftijd;

    public Persoon(String naam, Geslacht geslacht, int leeftijd) {
        this.naam = naam;
        this.geslacht = geslacht;
        this.leeftijd = leeftijd;
    }

    public String getNaam() {
        return naam;
    }

    public Geslacht getGeslacht() {
        return geslacht;
    }

    public int getLeeftijd() {
        return leeftijd;
    }

    public int leeftijdsverschil(final Persoon andere) {
        return leeftijd - andere.leeftijd;
    }

    @Override
    public String toString() {
        return String.format("%-9s - %s - %2d", naam, geslacht, leeftijd);
    }
}
