package robomail;

import employee.Employee;
import employee.Gender;
import employee.Role;

import java.util.List;
import java.util.function.Predicate;

public class RoboMailNewStyle {

    public void mail(List<Employee> pl, Predicate<Employee> pred) {
        for (Employee e : pl) {
            if (pred.test(e)) {
                roboMail(e);
            }
        }
    }

    private void roboMail(Employee p) {
        System.out.println("Emailing: " + p.getGivenName()
                + " " + p.getSurName() + " age " + p.getAge()
                + " at " + p.getEmail());
    }

}
