package be.kdg;

import be.kdg.sorteren.Piloot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author Kristiaan Behiels
 * @version 1.0 6/11/2015 20:32
 *
 *          Maak om te sorteren gebruik van de Stream<T> Interface methode
 *          sorted Gebruik de methode met de signatuur: Stream
 *          <T> sorted(Comparator<? super T> comparator)
 */
public class TestSorteren {
	public static void main(String[] args) {
		List<Piloot> drivers = Arrays.asList(new Piloot("Hamilton", 44), new Piloot("Rosberg", 6),
				new Piloot("Vettel", 5), new Piloot("Räikkönen", 7), new Piloot("Bottas", 77), new Piloot("Massa", 19),
				new Piloot("Ricciarddo", 3), new Piloot("Kvyat", 26));

		/* Gesorteerd volgens oplopend nummer (zonder implements Comparable) */
		drivers.stream().sorted(Comparator.comparing(Piloot::getNummer)).forEach(System.out::println);
		System.out.println();
		/* Gesorteerd volgens naam (zonder implements Comparable) */
		drivers.stream().sorted(Comparator.comparing(Piloot::getNaam)).forEach(System.out::println);

	}
}

/*
 * Ricciarddo 3 Vettel 5 Rosberg 6 Räikkönen 7 Massa 19 Kvyat 26 Hamilton 44
 * Bottas 77
 * 
 * Bottas 77 Hamilton 44 Kvyat 26 Massa 19 Ricciarddo 3 Rosberg 6 Räikkönen 7
 * Vettel 5
 */