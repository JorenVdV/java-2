package conversie;

import apen.Aap;
import apen.Geslacht;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class ConversieTools {

    public static List<Aap> readTxtFile(String bestand) throws IOException {
        List<Aap> list = new ArrayList<>();
        String regel = "";
        try (BufferedReader br = new BufferedReader(new FileReader(bestand))) {
            while ((regel = br.readLine()) != null) {
                //Elke regel in het bestand bevat de data van een aap, gescheiden door komma's
                StringTokenizer tokenizer = new StringTokenizer(regel, ",");

                String naam = tokenizer.nextToken();
                String soort = tokenizer.nextToken();
                String familie = tokenizer.nextToken();
                Geslacht geslacht = tokenizer.nextToken().charAt(0) == 'M' ? Geslacht.MAN : Geslacht.VROUW;
                LocalDate geboorte = LocalDate.parse(tokenizer.nextToken());
                double gewicht = Double.parseDouble(tokenizer.nextToken());
                String kooi = tokenizer.nextToken();

                // Nieuwe aap maken en toevoegen aan de List:
                list.add(new Aap(naam, soort, familie, geslacht, geboorte, gewicht, kooi));
            }
            return list;
        } catch (NoSuchElementException | NumberFormatException e1) {
            throw new IOException("Leesfout in regel: " + regel, e1);
        } catch (IOException e2) {
            throw new IOException("Het bronbestand " + bestand + " kan niet geopend worden", e2);
        }
    }

    public static String JdomWriteXML(List<Aap> myList, String fileName) throws IOException {
        //Opgave 1C
    	try{
	    	Element apen = new Element("apen");
			Document doc = new Document(apen);
			//doc.setRootElement(apen);
			
			for(Aap aap: myList){
				Element aapel = new Element("aap");
				aapel.setAttribute("kooi", aap.getKooi());
				
				Element naam = new Element("naam");
				naam.addContent(aap.getNaam());
				aapel.addContent(naam);
				Element soort = new Element("soort");
				soort.addContent(aap.getSoort());
				aapel.addContent(soort);
				Element geslacht = new Element("geslacht");
				geslacht.addContent(aap.getGeslacht().toString());
				aapel.addContent(geslacht);
				Element gewicht = new Element("gewicht");
				gewicht.addContent(Double.toString(aap.getGewicht()));
				aapel.addContent(gewicht);
				
				apen.addContent(aapel);
			}
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(doc, new FileWriter(fileName));
			
			return xmlOutput.outputString(doc);
    	}catch(IOException io) {
    		System.out.println(io.getMessage());
  	  	}	
		
        return "";
    }
}
