import apen.Aap;
import apen.Apen;
import apen.Geslacht;
import conversie.ConversieTools;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DemoApen {
    public static void main(String[] args) {
        try {
            Apen apen = ConversieTools.JaxbReadXML("AlleApen.xml");
            List<Aap> myApenList = apen.getApenList();

            //Opgave 2B:
            Map<String, List<Aap>> soortMap = myApenList.stream()
            		.sorted()
            		.collect(Collectors.groupingBy(Aap::getSoort));
            
            soortMap.keySet().stream().sorted()
            	.forEach(a->System.out.println(a+" -> "
            					+soortMap.get(a)
            					.stream()
            					.map(Aap::getNaam)
            					.collect(Collectors.joining(", "))));

            //Opgave 2C:
            Aap zwaarste = myApenList.stream()
            		.sorted(Comparator.comparing(Aap::getGewicht).reversed())
            		.findFirst().get();
            System.out.printf("\nZwaarste mannetjesaap: %s = %.1f kg\n"
                    , zwaarste.getNaam(), zwaarste.getGewicht());

            ConversieTools.StaxWriteXML(soortMap, "ApenPerSoort");

        } catch (JAXBException | FileNotFoundException | XMLStreamException e) {
            System.out.println(e.getMessage());
        }
    }
}

/**
 * GEWENSTE AFDRUK:
 *
 Alle apen alfabetisch per soort:
 bonobo   -> Koko, Pipi
 brulaap  -> Grompy, Shout
 doodshoofdaap -> Bumba, Griezel
 gorilla  -> Gust, Pinky, Sneeuwvlokje
 leeuwaap -> Java, Monkey
 maki     -> Banana, Nikita
 neusaap  -> Nancy, Pinokkio
 orang-oetan -> Kingkong, Louie, Rosa

 Zwaarste mannetjesaap: Gust = 175,0 kg
 File saved!
 */
