package apen;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "APEN")
public class Apen {
    private List<Aap> apenList = new ArrayList<>();

    @XmlElement(name = "AAP")
    public void setApenList(List<Aap> apenList) {
        this.apenList = apenList;
    }

    public List<Aap> getApenList() {
        return apenList;
    }

    public void add(Aap aap) {
        this.apenList.add(aap);
    }
}
