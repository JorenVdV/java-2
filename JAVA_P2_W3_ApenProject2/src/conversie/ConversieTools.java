package conversie;

import apen.Aap;
import apen.Apen;
import apen.Geslacht;
import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.*;

public class ConversieTools {

    public static Apen JaxbReadXML(String fileName) throws JAXBException {
        //Opgave 2A
    	try {
    		JAXBContext jc = JAXBContext.newInstance(Apen.class); 
    		Unmarshaller u = jc.createUnmarshaller();
    		File f = new File(fileName);
    		Apen apen = (Apen) u.unmarshal(f);
    		return apen;
    	} catch (JAXBException e) {
    		e.printStackTrace();
    	}
        return null;
    }

    public static void StaxWriteXML(Map<String, List<Aap>> myMap, String fileName) throws XMLStreamException, FileNotFoundException {
        PrintWriter writerXml = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(writerXml);
        //Specifieke xmlStreamWriter om indenting in de XML-output in te voegen:
        xmlStreamWriter = new IndentingXMLStreamWriter(xmlStreamWriter);
        
        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement("apen");

        //Opgave 2D
        for(String soort: myMap.keySet()){
        	xmlStreamWriter.writeStartElement("soort");
        	xmlStreamWriter.writeCharacters(soort);
        	for(Aap aap:myMap.get(soort)){
        		xmlStreamWriter.writeStartElement("aap");
        		
        		xmlStreamWriter.writeStartElement("naam");
        		xmlStreamWriter.writeCharacters(aap.getNaam());
        		xmlStreamWriter.writeEndElement(); //naam
        		
        		xmlStreamWriter.writeStartElement("gewicht");
        		xmlStreamWriter.writeCharacters(Double.toString(aap.getGewicht()));
        		xmlStreamWriter.writeEndElement(); //gewicht
        		
        		xmlStreamWriter.writeStartElement("geboorte");
        		xmlStreamWriter.writeCharacters(aap.getGeboorte().toString());
        		xmlStreamWriter.writeEndElement(); //geboorte
        		
        		xmlStreamWriter.writeStartElement("kooi");
        		xmlStreamWriter.writeCharacters(aap.getKooi());
        		xmlStreamWriter.writeEndElement(); //kooi       		
        		
        		xmlStreamWriter.writeEndElement(); // aap
        	}
        	
        	xmlStreamWriter.writeEndElement(); // soort
        }
        xmlStreamWriter.writeEndElement(); //apen

        xmlStreamWriter.flush();
        xmlStreamWriter.close();
        writerXml.close();

        System.out.println("File saved!");
    }
}
