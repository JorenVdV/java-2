package conversie;

import apen.Aap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.*;
import java.util.*;

public class ConversieTools {

    public static List<Aap> GsonReadList(String fileName) throws FileNotFoundException {
        //Opgave 3A
    	Gson gson = new Gson();
    	try (BufferedReader data = new BufferedReader(new FileReader(fileName))) {
            Aap[] personArray = gson.fromJson(data, Aap[].class);
            List<Aap> otherList = Arrays.asList(personArray);
            return otherList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void GsonWriteList(List<Aap> apenList, String fileName) throws FileNotFoundException {
        //Opgave 3D
    	try (PrintWriter jsonWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fileName)))) {
    		
    		GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
    		String jsonString = gson.toJson(apenList);
    		
    		System.out.println(jsonString);
    		
    		jsonWriter.write(jsonString);
            jsonWriter.close();
            System.out.println("Json file saved");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
