package be.kdg.jdom;

import be.kdg.model.Piloot;
import be.kdg.model.Piloten;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class MaakXML {
    private static final String FILENAME = "jdom.xml";

    public static void main(String[] args) {
        List<Piloot> piloten = Piloten.getPiloten();

        try {
            Element rootElement = new Element("piloten");
            Document document = new Document(rootElement);

            voegPilootElementenToe(piloten, rootElement);

            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());

            xmlOutput.output(document, System.out);
            xmlOutput.output(document, new FileWriter(FILENAME));
            System.out.println("\n" + FILENAME + "opgeslagen!");

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    private static void voegPilootElementenToe(List<Piloot> piloten, Element rootElement) {
        // Opdracht 1A
    	for (Piloot piloot : piloten) {
			Element el = new Element("piloot");
			el.setAttribute("team", piloot.getTeam());
			Element naam = new Element("naam");
			naam.addContent(piloot.getNaam());
			el.addContent(naam);
			Element nummer = new Element("nummer");
			nummer.addContent(Integer.toString(piloot.getNummer()));
			el.addContent(nummer);
			rootElement.addContent(el);
		}


    }
}

/*
<?xml version="1.0" encoding="UTF-8"?>
<piloten>
  <piloot team="Mercedes">
    <naam>Hamilton</naam>
    <nummer>44</nummer>
  </piloot>
  <piloot team="Mercedes">
    <naam>Rosberg</naam>
    <nummer>6</nummer>
  </piloot>
  <piloot team="Ferrari">
    <naam>Vettel</naam>
    <nummer>5</nummer>
  </piloot>
  <piloot team="Ferrari">
    <naam>Räikkönen</naam>
    <nummer>7</nummer>
  </piloot>
  <piloot team="Williams">
    <naam>Bottas</naam>
    <nummer>77</nummer>
  </piloot>
  <piloot team="Williams">
    <naam>Massa</naam>
    <nummer>19</nummer>
  </piloot>
  <piloot team="Red Bull">
    <naam>Ricciarddo</naam>
    <nummer>3</nummer>
  </piloot>
  <piloot team="Red Bull">
    <naam>Kvyat</naam>
    <nummer>26</nummer>
  </piloot>
</piloten>

jdom.xmlopgeslagen!
 */
