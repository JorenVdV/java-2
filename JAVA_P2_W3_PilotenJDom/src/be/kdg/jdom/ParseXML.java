package be.kdg.jdom;

import be.kdg.model.Piloot;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ParseXML {
    private static final String FILENAME = "jdom.xml";

    public static void main(String[] args) {
        try {
            File inputFile = new File(FILENAME);
            SAXBuilder saxBuilder = new SAXBuilder();

            Document document = saxBuilder.build(inputFile);
            Element rootElement = document.getRootElement();

            // Opdracht 1B
            

        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
    }
}

/*
Overzicht piloten:

Mercedes Hamilton   44
Mercedes Rosberg    6
Ferrari  Vettel     5
Ferrari  Räikkönen  7
Williams Bottas     77
Williams Massa      19
Red Bull Ricciarddo 3
Red Bull Kvyat      26
 */
