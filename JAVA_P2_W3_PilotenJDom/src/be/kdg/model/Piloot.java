package be.kdg.model;

/**
 * @author Kristiaan Behiels
 * @version 1.0 28/11/2015 15:37
 */
public class Piloot {
    private String team;
    private String naam;
    private int nummer;

    public Piloot(String team, String naam, int nummer) {
        this.team = team;
        this.naam = naam;
        this.nummer = nummer;
    }

    public String getTeam() {
        return team;
    }

    public String getNaam() {
        return naam;
    }

    public int getNummer() {
        return nummer;
    }

    @Override
    public String toString() {
        return String.format("%-8s %-10s %d", team, naam, nummer);
    }
}
