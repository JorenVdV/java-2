package be.kdg.model;

import java.util.Arrays;
import java.util.List;

/**
 * @author Kristiaan Behiels
 * @version 1.0 28/11/2015 15:43
 */
public class Piloten {
    private static final List<Piloot> piloten = Arrays.asList(
            new Piloot("Mercedes", "Hamilton", 44),
            new Piloot("Mercedes", "Rosberg", 6),
            new Piloot("Ferrari", "Vettel", 5),
            new Piloot("Ferrari", "Räikkönen", 7),
            new Piloot("Williams", "Bottas", 77),
            new Piloot("Williams", "Massa", 19),
            new Piloot("Red Bull", "Ricciarddo", 3),
            new Piloot("Red Bull", "Kvyat", 26));

    public static List<Piloot> getPiloten() {
        return piloten;
    }
}
