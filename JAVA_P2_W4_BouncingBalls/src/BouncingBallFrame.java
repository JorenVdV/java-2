import bouncing.BallLauncher;

import javax.swing.*;
import java.awt.*;

// Event handling aan te vullen!
public class BouncingBallFrame extends JFrame {
    private JPanel canvas;
    private JButton start;
    private JButton freeze;
    private JButton exit;
    private BallLauncher balls = new BallLauncher();

    public BouncingBallFrame() {
        super("Bouncing Balls");
        setResizable(false);
        maakComponenten();
        maakLayout();
        voegListenersToe();
        toonFrame();
    }

    private void maakComponenten() {
        canvas = new JPanel();
        canvas.setBackground(Color.DARK_GRAY);
        start = new JButton("Launch ball");
        freeze = new JButton("Freeze ball");
        exit = new JButton("Exit");
    }

    private void maakLayout() {
        JPanel panel = new JPanel(new GridLayout(1, 3));
        panel.add(start);
        panel.add(freeze);
        panel.add(exit);
        JPanel knoppenPanel = new JPanel(new FlowLayout());
        knoppenPanel.add(panel);
        add(canvas, BorderLayout.CENTER);
        add(knoppenPanel, BorderLayout.SOUTH);
    }

    // Hier aan te vullen (gebruik Lambda's)
    private void voegListenersToe() {
        exit.addActionListener(e -> {
            canvas.setVisible(false);
            System.exit(0);
        });
        start.addActionListener(e -> {
        	balls.launch(canvas);
        	System.out.println("added ball");
        });
        freeze.addActionListener(e -> {
        	balls.freeze();
        	System.out.println("Froze ball");
        });
    }

    private void toonFrame() {
        setSize(600, 400);
        // pack();
        setLocationRelativeTo(getRootPane());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
