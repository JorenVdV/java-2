package bouncing;

import javax.swing.*;
import java.awt.*;

public class Ball extends Thread {
	private Color color;
	private JPanel box;
	private Graphics graphics;

	private int diameter = 30;
	private int xPos;
	private int yPos;
	private int deltaX = 5;
	private int deltaY = 5;

	public Ball(JPanel panel, Color color, int xPos, int yPos) {
		box = panel;
		this.color = color;
		this.xPos = xPos;
		this.yPos = yPos;
		graphics = box.getGraphics();
	}

	public void move() {
		// System.out.println("RUNNING
		// THREAD:"+Thread.currentThread().getName());
		if (!box.isVisible()) {
			return;
		}

		// wegvegen oude bal:
		graphics.setColor(box.getBackground());
		graphics.fillOval(xPos, yPos, diameter, diameter);

		// berekenen nieuwe positie:
		xPos += deltaX;
		yPos += deltaY;

		Dimension d = box.getSize();
		if (xPos < 0) {
			xPos = 0;
			deltaX = -deltaX;
		}
		if (xPos + diameter >= d.width) {
			xPos = d.width - diameter;
			deltaX = -deltaX;
		}
		if (yPos < 0) {
			yPos = 0;
			deltaY = -deltaY;
		}
		if (yPos + diameter >= d.height) {
			yPos = d.height - diameter;
			deltaY = -deltaY;
		}

		// tekenen nieuwe bal:
		graphics.setColor(color);
		graphics.fillOval(xPos, yPos, diameter, diameter);
	}

	@Override
	public void run() {
		while (true) {
			if (Thread.currentThread().isInterrupted()) {
				//wegvegen oude bal:

				break;
			}
			if (!Thread.currentThread().isInterrupted()) {
				move();
				yield();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
//					System.out.println("panic");
					//e.printStackTrace();
					break;
				}
			}
		}
	}

	public void paintblank() {
		graphics.setColor(box.getBackground());
		graphics.fillOval(xPos, yPos, diameter, diameter);
	}
}
