package bouncing;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BallLauncher {
    private static final Random random = new Random();
    private static List<Ball> balls = new ArrayList<>();

    // Aan te vullen
    
    public void launch(JPanel panel){
    	Color color = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
    	Ball ball = new Ball(panel, color, 
    			random.nextInt(panel.getWidth()), random.nextInt(panel.getHeight()));
    	balls.add(ball);
    	ball.setDaemon(true);
    	ball.start();
    }
    
    public void freeze(){
    	if(balls.size()==0)return;
    	Ball ball = balls.get(balls.size()-1);
    	ball.interrupt();
    	try {
			balls.get(balls.size()-1).join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	ball.paintblank();
    	balls.remove(balls.size()-1);
    }
    
    public int getCount(){
    	return balls.size();
    }
}
