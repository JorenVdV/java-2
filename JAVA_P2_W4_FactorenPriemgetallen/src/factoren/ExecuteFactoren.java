package factoren;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExecuteFactoren {
	private static final long DEMOGETAL = 214_577_422_401L;
	public static boolean prime = true;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); 
		System.out.print("Geef een getal: "); 
		long getal = DEMOGETAL;
		
		try {
            getal = scanner.nextLong();
         } catch (InputMismatchException e) {
            // foutieve invoer, we nemen het demogetal
         }
         // Hier aanvullen!
		long number_of_threads = Math.round(Math.sqrt(getal)/100 + 1.0);
		for(int i=0; i<number_of_threads;i++){
			Thread tr = new Thread(new FactorenRunnable(i*100, getal));
			tr.start();
			try {
				tr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(prime)System.out.println("Dit is een priemgetal!");
	}
            
}
