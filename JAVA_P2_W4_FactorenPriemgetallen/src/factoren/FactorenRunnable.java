package factoren;

public class FactorenRunnable implements Runnable {
	private static long getal;
	private static int aantalFactoren = 0;
	private int begin;
	private int einde;

	public FactorenRunnable(int begin, long teOnderzoekenGetal) {
		if (begin == 0) {
			this.begin = 2;
		} else {
			this.begin = begin;
		}
		getal = teOnderzoekenGetal;
		einde = begin + 99;
	}

	// Vul hier aan!
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i=begin; i<=einde; i++){
			if(getal%i==0){
				System.out.println("Factor "+Integer.toString(i)+" Thread-"+
						Thread.currentThread().getName());
				ExecuteFactoren.prime=false;
			}
		
		}
	}
}
