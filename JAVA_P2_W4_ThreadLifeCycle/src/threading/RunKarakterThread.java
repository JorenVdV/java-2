package threading;

import java.util.Scanner;

public class RunKarakterThread {
	public static void main(String[] args) throws InterruptedException {
		int[] counter = { 0 }; // Trucje om een int schijnbaar effectively final
		Runnable myRunnable = () -> {
			while (true) {
				System.out.print((char) ('a' + counter[0]++) + " ");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					//System.err.println(e.toString());
					break; // spring uit de loop }
				}
			}
			System.out.println("Thread beëindigd");
		};
		// vul hier aan
		Thread tr1 = new Thread(myRunnable);
		System.out.println("State: "+tr1.getState()+" Alive: "+tr1.isAlive());
		tr1.setDaemon(true);
		tr1.start();
		System.out.println("State: "+tr1.getState()+" Alive: "+tr1.isAlive());
		
		System.out.println("Druk op <ENTER> om te stoppen...");
        new Scanner(System.in).nextLine();
        tr1.interrupt();
        System.out.println("State: "+tr1.getState()+" Alive: "+tr1.isAlive());
     	tr1.join();
        System.out.println("State: "+tr1.getState()+" Alive: "+tr1.isAlive());
		System.out.println("Einde programma");
	}
}
