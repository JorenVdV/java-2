package carwash;

public class Carwash {
	private boolean occupied = false;

	public synchronized void aankomstWagen(int wagennr) {
		if (occupied) {
			System.out.println("Wagen nr." + wagennr + " moet wachten");
			while (occupied) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
		}

		occupied = true;
		System.out.println("Start wagen nr." + wagennr);

	}

	public synchronized void vertrekWagen(int wagennr) {
		occupied = false;
		System.out.println("Wagen nr." + wagennr + " vertrokken");
		notifyAll();
	}
}
