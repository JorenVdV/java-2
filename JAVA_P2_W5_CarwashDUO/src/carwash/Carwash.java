package carwash;

public class Carwash {
	private int occupied = 0;

	public synchronized void aankomstWagen(int wagennr) {
		if (occupied>1) {
			System.out.println("Wagen nr." + wagennr + " moet wachten");
			while (occupied>1) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
		}

		occupied++;
		System.out.println("Start wagen nr." + wagennr);

	}

	public synchronized void vertrekWagen(int wagennr) {
		occupied--;
		System.out.println("Wagen nr." + wagennr + " vertrokken");
		notifyAll();
	}
}
