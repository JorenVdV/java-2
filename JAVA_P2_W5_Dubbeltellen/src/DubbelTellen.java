
public class DubbelTellen {
	public static void main(String[] args) {
		Object obj = new Object();

		Thread tel1 = new Thread(() -> {
			synchronized (obj) {
				for (int i =1;i <= 10;++i) {
					System.out.print(i + " ");
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {

					}
				}
			}
		});

		Thread tel2 = new Thread(() -> {
			synchronized (obj) {
				for (int i =1;i <= 10;++i) {
					System.out.print(i + " ");
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {

					}
				}
			}
		});

		tel1.start();
		tel2.start();
	}
}
