public class Besturing extends Thread {
	private static final int TOENAME = 15;

	public void verhoogDruk(int toename) {
		synchronized (Stoomketel.class) {
			if (Stoomketel.getDrukWaarde() < Stoomketel.getAlarmWaarde() - TOENAME) {
				try {
					sleep(100);
				} catch (InterruptedException e) {
					// negeer
				}
				Stoomketel.verhoogDruk(toename);
			}
		}
	}

	public void run() {
		verhoogDruk(TOENAME);
	}
}