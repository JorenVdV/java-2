public class Stoomketel {
	private static final int ALARMWAARDE = 20;
	private static int drukwaarde = 0;

	public static int getAlarmWaarde() {
		return ALARMWAARDE;
	}

	public static int getDrukWaarde() {
		return drukwaarde;
	}

	public static void verhoogDruk(int toename) {
		drukwaarde += toename;
	}
}