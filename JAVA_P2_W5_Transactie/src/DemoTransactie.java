public class DemoTransactie{
	public static void main(String[] args){
		Thread plus = new Thread(new Transactie(100000,true));
		Thread min = new Thread(new Transactie(100000,false));
		
		System.out.println("\n Threads gestart...");
		plus.start();
		min.start();
		try{
			plus.join();
			min.join();
		}catch (InterruptedException e){}
		System.out.println("Threads beeindigd\n");
		System.out.println("Saldo: "+Transactie.getSaldo());
	}
}