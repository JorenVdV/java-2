public class Transactie implements Runnable {
	private static int saldo = 0;
	private static final Object lock= new Object();   
	private int aantal;
	private boolean actie;

	public Transactie(int aantalKeer, boolean actie) {
		aantal = aantalKeer;
		this.actie = actie;
	}

	public static int getSaldo() {
		return saldo;
	}

	public void run() {
		for (int i = 0; i < aantal; ++i) {
			if (actie) {
				synchronized (lock) {
					saldo = saldo + 10;
				}
			} else {
				synchronized (lock) {
					saldo = saldo - 10;
				}
			}
		}
	}
}