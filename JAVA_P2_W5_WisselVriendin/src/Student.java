
public class Student implements Runnable {
	private final String naam;
	private final String oudeVriendin;
	private final String nieuweVriendin;

	public Student(String naam, String oudeVriendin, String nieuweVriendin) {
		this.naam = naam;
		this.oudeVriendin = oudeVriendin;
		this.nieuweVriendin = nieuweVriendin;
	}

	public void run() {
		System.out.println(naam + " belt " + oudeVriendin);
		synchronized (oudeVriendin) {
			System.out.println(naam + " maakt het af met " + oudeVriendin);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// negeer
			}
		}
		System.out.println(naam + " belt " + nieuweVriendin);
		synchronized (nieuweVriendin) {
			System.out.println(naam + " vraagt het aan bij " + nieuweVriendin);
		}
		System.out.println(naam + " heeft een nieuwe vriendin: " + nieuweVriendin);
	}
}
