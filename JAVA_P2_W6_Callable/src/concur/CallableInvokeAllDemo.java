package concur;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableInvokeAllDemo {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(3);
		List<Future<Long>> results = null; 
		try {
			results = executor.invokeAll(new Taken().getTaken()); 
		} catch (InterruptedException e) {
			e.printStackTrace(); // Voor testfase. }
			executor.shutdown(); // Niet vergeten!
		}
		// Hier moet je met behulp van een lus het resultaat van
		// elke taak onder mekaar afdrukken
		for(Future<Long> result: results){
			System.out.println(result.get());
		}
	}
}
	