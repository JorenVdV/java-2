package concur;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Som implements Callable<Long> {
	private final long beginWaarde;
	private final long eindWaarde;

	public Som(long beginWaarde, long eindWaarde) {
		this.beginWaarde = beginWaarde;
		this.eindWaarde = eindWaarde;
	}
	// Hier moet een functie komen die de som van alle
	// getallen vanaf de beginwaarde teruggeeft
	@Override
	public Long call() throws Exception {		
		return LongStream.rangeClosed(beginWaarde, eindWaarde).boxed().collect(Collectors.toList())
				.stream().reduce((a,b)->a+b).get().longValue();
	}
}
