package concur;

import java.util.ArrayList;
import java.util.List;

public class Taken {
	private List<Som> taken = new ArrayList<>();

	public Taken() {
		taken.add(new Som(1, 10));
		taken.add(new Som(1, 100));
		taken.add(new Som(1, 1000));
		taken.add(new Som(1, 10_000));
		taken.add(new Som(1, 100_000));
		taken.add(new Som(1, 1_000_000));
	}

	public List<Som> getTaken() {
		return taken;
	}
}
