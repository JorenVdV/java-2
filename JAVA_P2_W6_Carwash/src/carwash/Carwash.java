package carwash;

import java.util.concurrent.ArrayBlockingQueue;

public class Carwash {
	private final ArrayBlockingQueue<Integer> carwash;

	public Carwash() {
		carwash = new ArrayBlockingQueue<>(2);
	}

	public void aankomstWagen(int nr) {
		// Vul aan
		try {
			if(carwash.remainingCapacity()==0)
				System.out.println("Wagen nr." + nr + " moet wachten");
			carwash.put(nr);
		} catch (InterruptedException e) {
		}
		System.out.println("Start wagen nr." + nr);
	}

	// Voorzie in deze methode een 100ms sleep om System.out 
	// voorrang te geven
	public void vertrekWagen(int nr) {
		// Vul aan 
		try {
			Thread.sleep(100);
			if(carwash.take()!=nr)System.err.println("error");
			System.out.println("Wagen nr." + nr + " klaar");
		} catch (InterruptedException e) {
		}
		
	}
}