import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Fibonacci {
	private ConcurrentMap<Integer, Integer> values = new ConcurrentHashMap<>();

	public int calculate(int x) {
		if (x < 0) {
			throw new IllegalArgumentException("positive numbers only");
		}
		if (x <= 1) {
			return x;
		}
		return calculate(x - 1) + calculate(x - 2);
	}

	public int calculateWithCache(int x) {
		Integer result = values.get(x);
		if (result == null) {
			result = calculate(x);
			values.putIfAbsent(x, result);
		}
		return result;
	}

	public int calculateOnlyWithCache(int x) {
		Integer first = values.get(x - 1);
		Integer second = values.get(x - 2);
		Integer result = values.get(x);
		if (result != null) {
			return result;
		}
		if ((first == null) || (second == null)) {
			throw new IllegalArgumentException("values not in cache");
		}
		result = first + second;
		values.putIfAbsent(x, result);
		return result;
	}

	public void calculateRangeInCache(int x, int y) {
		calculateWithCache(x++);
		calculateWithCache(x++);
		while (x <= y) {
			calculateOnlyWithCache(x++);
		}
	}
}
