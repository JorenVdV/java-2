import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class FibonacciConsumer implements Runnable {
	private Fibonacci fibonacci = new Fibonacci();
	private BlockingQueue<Integer> queue;

	public FibonacciConsumer(BlockingQueue<Integer> queue) {
		this.queue = queue;
		Thread tr = new Thread(this);
		tr.start();
	}

	public void run() {
		int request = 0;
		int result = 0;
		while (true){
			try {
				request = queue.poll(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
			}
			result = fibonacci.calculate(request);
			System.out.printf("Calculated result of %d from %d\n", result, request);
			Thread.yield();
		}
	}
}
