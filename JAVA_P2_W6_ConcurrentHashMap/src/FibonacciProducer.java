import java.util.concurrent.BlockingQueue;

public class FibonacciProducer implements Runnable {
	private BlockingQueue<Integer> queue;
	private final int LIMIT=100;

	public FibonacciProducer(BlockingQueue<Integer> queue) {
		// Hier moet je een thread maken op basis van this en hem starten
		this.queue = queue;
		Thread tr = new Thread(this);
		tr.start();
	}

	@Override
	public void run() {
		// Voorzie een sleep van 1s
		int count = 0;
		while(count <LIMIT){
			try {
				queue.put(count);
				System.out.printf("Produced request %d\n", count);
				count++;
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}

}
