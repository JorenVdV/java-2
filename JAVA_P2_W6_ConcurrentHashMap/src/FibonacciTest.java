import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class FibonacciTest {
	public static void main(String[] args) {
		BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);
		new FibonacciProducer(queue);
		for (int i = 0; i < 2; i++) {
			new FibonacciConsumer(queue);
		}
	}
}