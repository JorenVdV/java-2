import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// deel1();
		// deel2();
		deel3();
	}

	private static void deel1() {
		List<Integer> getallen = Stream.iterate(1, n -> n + 1).limit(10).collect(Collectors.toList());
		getallen.parallelStream().forEach(System.out::println);
		System.out.println("----------------------------------------------------------------");
		List<Integer> collected = getallen.parallelStream().collect(Collectors.toList());
		collected.stream().forEach(System.out::println);

		String string = getallen.parallelStream().map(i -> Integer.toString(i)).collect(Collectors.joining(", "));
		System.out.println("----------------------------------------------------------------");
		System.out.println(string);
	}

	private static void deel2() {
		List<Integer> getallen = IntStream.rangeClosed(1, 10000).boxed().collect(Collectors.toList());
		int totaal = getallen.parallelStream().reduce(0, (a, b) -> a + b);
		System.out.println(totaal);
		System.out.println("----------------------------------------------------------------");
		long count = getallen.parallelStream().filter(x -> (x % 15 == 0)).count();
		System.out.println(count);
		System.out.println("----------------------------------------------------------------");

		final int[] counter = { 0 };
		getallen.parallelStream().map(e -> {
			counter[0] += 1;
			return e;
		}).reduce((a, b) -> a);
		System.out.println("Counter: " + counter[0]);

	}

	private static void deel3() throws InterruptedException, ExecutionException {
		List<Integer> getallen = IntStream.rangeClosed(1, 10000).boxed().collect(Collectors.toList());
		ForkJoinPool pool = new ForkJoinPool(4);
		final int[] counter = { 0 };
		pool.submit(() -> getallen.parallelStream().forEach(e -> counter[0]++)).get();
		System.out.println("Counter: " + counter[0]);

	}
}
