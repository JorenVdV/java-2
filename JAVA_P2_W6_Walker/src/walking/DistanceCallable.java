package walking;

import java.util.concurrent.Callable;

public class DistanceCallable implements Callable<Integer>{
    private Walker walker;
    public static final int STEPS = 1000;

    public DistanceCallable(Walker walker) {
        this.walker = walker;
    }
    private enum DIR {FORWARD, BACKWARD};
	@Override
	public Integer call() throws Exception {
		int steps=0;
		for(int i=0; i<STEPS; i++){
			steps+= walker.getForward()-walker.getBackward();				
		}
		return steps;
	}

    // Werk verder uit

}
